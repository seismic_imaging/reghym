!=====================================================================
!
!               S p e c f e m 3 D  V e r s i o n  3 . 0
!               ---------------------------------------
!
!     Main historical authors: Dimitri Komatitsch and Jeroen Tromp
!                              CNRS, France
!                       and Princeton University, USA
!                 (there are currently many more authors!)
!                           (c) October 2017
!
! This program is free software; you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program; if not, write to the Free Software Foundation, Inc.,
! 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
!
!=====================================================================


! *********************************************************************************
!
! coupling with an injection boundary (FK, DSM, AxiSEM, ..)
!
! some routines here were added by Ping Tong (TP / Tong Ping) for the FK3D calculation.
!
! when using this technique, please also reference the original work:
!
! "Three-dimensional full waveform inversion of short-period teleseismic wavefields based upon the SEM-DSM hybrid method"
! Vadim Monteiller, Sebastien Chevrot, Dimitri Komatitsch, Yi Wang
! Geophysical Journal International, Volume 202, Issue 2, 1 August 2015, Pages 811-827,
! https://doi.org/10.1093/gji/ggv189
!
!
! "A 3-D spectral-element and frequency-wave number hybrid method for high-resolution seismic array imaging"
! Tong, P; Komatitsch, D; Tseng, TL; Hung, SH; Chen, CW; Basini, P; Liu, QY
! GEOPHYSICAL RESEARCH LETTERS, Volume: 41  Issue: 20,  Pages: 7025-7034, OCT 28 2014, DOI: 10.1002/2014GL061644
! http://onlinelibrary.wiley.com/doi/10.1002/2014GL061644/abstract
!
!
! "High-resolution seismic array imaging based on an SEM-FK hybrid method"
! Ping Tong, Chin-wu Chen, Dimitri Komatitsch, Piero Basini and Qinya Liu
! Geophysical Journal International, Volume 197, Issue 1, 1 April 2014, Pages 369-395,
! https://doi.org/10.1093/gji/ggt508
!
! *********************************************************************************


subroutine couple_with_injection_setup()

    use specfem_par
    use specfem_par_coupling
    use specfem_par_elastic, only: ispec_is_elastic
    implicit none

    ! local parameters
    integer :: ier
    character(len = MAX_STRING_LEN) :: dsmname
    ! timing
    double precision :: tstart, tCPU
    double precision, external :: wtime
    real(kind=CUSTOM_REAL),dimension(:,:), allocatable :: tmp_read_array1, tmp_read_array2
    integer, dimension(num_abs_boundary_faces * NGLLSQUARE) :: ipt_elastic
    integer :: ipt, iface, ispec, igll
    
    ! for coupling with EXTERNAL CODE !! CD CD modify here
    if (COUPLE_WITH_INJECTION_TECHNIQUE .or. SAVE_RUN_BOUN_FOR_KH_INTEGRAL) then
        if (myrank == 0) then
            write(IMAIN, *)
            write(IMAIN, *) '**********************************************'
            write(IMAIN, *) '      **** USING HYBRID METHOD  ****'
            write(IMAIN, *) '**********************************************'
            write(IMAIN, *)
            write(IMAIN, *) ' using coupling with injection technique:'
            select case(INJECTION_TECHNIQUE_TYPE)
            case (INJECTION_TECHNIQUE_IS_DSM)
                write(IMAIN, *) ' type of injection technique is DSM'
            case (INJECTION_TECHNIQUE_IS_AXISEM)
                write(IMAIN, *) ' type of injection technique is AXISEM'
            case (INJECTION_TECHNIQUE_IS_FK)
                write(IMAIN, *) ' type of injection technique is FK'
            case default
                stop 'Invalid INJECTION_TECHNIQUE_TYPE chosen, must be 1 == DSM, 2 == AXISEM or 3 == FK'
            end select
            write(IMAIN, *)
            write(IMAIN, *) '**********************************************'
            write(IMAIN, *)
        endif

        call create_name_database(dsmname, myrank, TRACTION_PATH)
    endif

    ! allocates arrays for coupling
    ! note: num_abs_boundary_faces needs to be set
    if (COUPLE_WITH_INJECTION_TECHNIQUE) then

        if (INJECTION_TECHNIQUE_TYPE == INJECTION_TECHNIQUE_IS_DSM) then

            allocate(Veloc_dsm_boundary(3, Ntime_step_dsm, NGLLSQUARE, num_abs_boundary_faces), stat = ier)
            if (ier /= 0) call exit_MPI_without_rank('error allocating array 2190')
            allocate(Tract_dsm_boundary(3, Ntime_step_dsm, NGLLSQUARE, num_abs_boundary_faces), stat = ier)
            if (ier /= 0) call exit_MPI_without_rank('error allocating array 2191')

            if (old_DSM_coupling_from_Vadim) then
                open(unit = IIN_veloc_dsm, file = dsmname(1:len_trim(dsmname)) // 'vel.bin', status = 'old', &
                action = 'read', form = 'unformatted', iostat = ier)
                open(unit = IIN_tract_dsm, file = dsmname(1:len_trim(dsmname)) // 'tract.bin', status = 'old', &
                action = 'read', form = 'unformatted', iostat = ier)
            else
                !! To verify for NOBU version (normally, remains empty)
            endif

        else if (INJECTION_TECHNIQUE_TYPE == INJECTION_TECHNIQUE_IS_AXISEM) then

           !! ON SUPPOSE QUE TOUS LES PROCS ON AU MOINS 1 POINT SUR LE BORD :: FIXIT 
           if (Axisem_spline_interp) then
              ! get MPI starting time for FK
              tstart = wtime()
              ! dummy arrays 
              allocate(Veloc_axisem(1, 1), stat = ier)
              if (ier /= 0) call exit_MPI_without_rank('error allocating array 2192')
              allocate(Tract_axisem(1, 1), stat = ier)
              if (ier /= 0) call exit_MPI_without_rank('error allocating array 2193')
              !! read meta data
               open(unit = IIN_veloc_dsm, file = dsmname(1:len_trim(dsmname)) // 'sol_axisem.dat', status = 'old', &
                    action = 'read', iostat = ier)
               write(*, *) 'OPENING ', dsmname(1:len_trim(dsmname)) // 'sol_axisem.dat'
               read( IIN_veloc_dsm, *) dt_axisem, NF_FOR_STORING, npt
               close(IIN_veloc_dsm)

               !! compute re-sampling rate
               !!  compute resampling rate
               !np_resampling = floor(dt_axisem / deltat)
               !write(*,*) " old DT ", DT, dt_axisem
               NP_RESAMP =  floor(dt_axisem / DT)
               !! update deltat
               DT = dt_axisem / real(NP_RESAMP, kind=CUSTOM_REAL)
               !write(*,*) " New DT ", DT
               
               open(unit = IIN_veloc_dsm, file = dsmname(1:len_trim(dsmname)) // 'sol_axisem', status = 'old', &
                    action = 'read', form = 'unformatted', iostat = ier)
              write(*, *) 'OPENING ', dsmname(1:len_trim(dsmname)) // 'sol_axisem'

              !! re-use of FK arrays
              allocate(vx_FK(npt), stat = ier)
              allocate(vy_FK(npt), stat = ier)
              allocate(vz_FK(npt), stat = ier)
              allocate(tx_FK(npt), stat = ier)
              allocate(ty_FK(npt), stat = ier)
              allocate(tz_FK(npt), stat = ier)
              
!!$              allocate(VX_t(npt, -NP_RESAMP:NF_FOR_STORING+NP_RESAMP), stat = ier)
!!$              allocate(VY_t(npt, -NP_RESAMP:NF_FOR_STORING+NP_RESAMP), stat = ier)
!!$              allocate(VZ_t(npt, -NP_RESAMP:NF_FOR_STORING+NP_RESAMP), stat = ier)
!!$              allocate(TX_t(npt, -NP_RESAMP:NF_FOR_STORING+NP_RESAMP), stat = ier)
!!$              allocate(TY_t(npt, -NP_RESAMP:NF_FOR_STORING+NP_RESAMP), stat = ier)
!!$              allocate(TZ_t(npt, -NP_RESAMP:NF_FOR_STORING+NP_RESAMP), stat = ier)

              allocate(VX_t(npt,  0:NF_FOR_STORING+NP_RESAMP), stat = ier)
              allocate(VY_t(npt,  0:NF_FOR_STORING+NP_RESAMP), stat = ier)
              allocate(VZ_t(npt,  0:NF_FOR_STORING+NP_RESAMP), stat = ier)
              allocate(TX_t(npt,  0:NF_FOR_STORING+NP_RESAMP), stat = ier)
              allocate(TY_t(npt,  0:NF_FOR_STORING+NP_RESAMP), stat = ier)
              allocate(TZ_t(npt,  0:NF_FOR_STORING+NP_RESAMP), stat = ier)
!!$                            
              VX_t(:,:)=0._CUSTOM_REAL
              VY_t(:,:)=0._CUSTOM_REAL
              VZ_t(:,:)=0._CUSTOM_REAL
              TX_t(:,:)=0._CUSTOM_REAL
              TY_t(:,:)=0._CUSTOM_REAL
              TZ_t(:,:)=0._CUSTOM_REAL

              !! read axisem injection field
              allocate(tmp_read_array1(3, NF_FOR_STORING))
              allocate(tmp_read_array2(3, NF_FOR_STORING))
              do ipt = 1, npt 
                 read(IIN_veloc_dsm) tmp_read_array1, tmp_read_array2
                 VX_t(ipt, 1:NF_FOR_STORING) = tmp_read_array1(1,:)
                 VY_t(ipt, 1:NF_FOR_STORING) = tmp_read_array1(2,:)
                 VZ_t(ipt, 1:NF_FOR_STORING) = tmp_read_array1(3,:)
                 TX_t(ipt, 1:NF_FOR_STORING) = tmp_read_array2(1,:)
                 TY_t(ipt, 1:NF_FOR_STORING) = tmp_read_array2(2,:)
                 TZ_t(ipt, 1:NF_FOR_STORING) = tmp_read_array2(3,:)
              end do
              close(IIN_veloc_dsm)

              if (GPU_MODE) then
                 ipt=0
                 do iface = 1, num_abs_boundary_faces
                    ispec = abs_boundary_ispec(iface)
                    if (ispec_is_elastic(ispec)) then
                       do igll=1, NGLLSQUARE
                          ipt=ipt+1
                          ipt_elastic((iface-1)*NGLLSQUARE + igll) = ipt
                       end do
                    end if
                 end do
                 
                 !! need to transpose velocity and tractions arrays
                 deallocate(tmp_read_array1)
                 allocate(tmp_read_array1(0:NF_FOR_STORING+NP_RESAMP, npt))

                 do ipt=1,npt
                    tmp_read_array1(0:NF_FOR_STORING+NP_RESAMP, ipt) = VX_t(ipt, 0:NF_FOR_STORING+NP_RESAMP)
                 end do
                 deallocate(VX_t)
                 allocate(VX_t(0:NF_FOR_STORING+NP_RESAMP, npt))
                 VX_t(:,:) = tmp_read_array1(:,:)

                 do ipt=1,npt
                    tmp_read_array1(0:NF_FOR_STORING+NP_RESAMP, ipt) = VY_t(ipt, 0:NF_FOR_STORING+NP_RESAMP)
                 end do
                 deallocate(VY_t)
                 allocate(VY_t(0:NF_FOR_STORING+NP_RESAMP, npt))
                 VY_t(:,:) = tmp_read_array1(:,:)

                 do ipt=1,npt
                    tmp_read_array1(0:NF_FOR_STORING+NP_RESAMP, ipt) = VZ_t(ipt, 0:NF_FOR_STORING+NP_RESAMP)
                 end do
                 deallocate(VZ_t)
                 allocate(VZ_t(0:NF_FOR_STORING+NP_RESAMP, npt))
                 VZ_t(:,:) = tmp_read_array1(:,:)

                 do ipt=1,npt
                    tmp_read_array1(0:NF_FOR_STORING+NP_RESAMP, ipt) = TX_t(ipt, 0:NF_FOR_STORING+NP_RESAMP)
                 end do
                 deallocate(TX_t)
                 allocate(TX_t(0:NF_FOR_STORING+NP_RESAMP, npt))
                 TX_t(:,:) = tmp_read_array1(:,:)

                 do ipt=1,npt
                    tmp_read_array1(0:NF_FOR_STORING+NP_RESAMP, ipt) = TY_t(ipt, 0:NF_FOR_STORING+NP_RESAMP)
                 end do
                 deallocate(TY_t)
                 allocate(TY_t(0:NF_FOR_STORING+NP_RESAMP, npt))
                 TY_t(:,:) = tmp_read_array1(:,:)

                 do ipt=1,npt
                    tmp_read_array1(0:NF_FOR_STORING+NP_RESAMP, ipt) = TZ_t(ipt, 0:NF_FOR_STORING+NP_RESAMP)
                 end do
                 deallocate(TZ_t)
                 allocate(TZ_t(0:NF_FOR_STORING+NP_RESAMP, npt))
                 TZ_t(:,:) = tmp_read_array1(:,:)
                 
              end if
              
              ! get MPI ending time for reading files 
              tCPU = wtime() - tstart
              
              if (myrank == 0) then
                 write(IMAIN, *)
                 write(IMAIN, *) "      AXISEM INJECTION WITH SPLINE IN FLY RESAMPLING " 
                 write(IMAIN, *)
                 write(IMAIN, *)
                 write(IMAIN, *) 'ALLOCATE SAVED ARRAYS', NF_FOR_STORING
                 write(IMAIN, *)
                 write(IMAIN, *) '  resampling rate                = ', NP_RESAMP
                 write(IMAIN, *)
                 write(IMAIN, *) '  new time step for simulation   = ', DT
                 write(IMAIN, *)
                 write(IMAIN, *) '  total number of points on boundary = ', npt
                 write(IMAIN, '(a35,1x, f20.2, a7)') " Elapsed time for reading axisem files  : ", tCPU, " sec. "
                 call flush_IMAIN()
              end if
              
           else 
              allocate(Veloc_axisem(3, NGLLSQUARE * num_abs_boundary_faces), stat = ier)
              if (ier /= 0) call exit_MPI_without_rank('error allocating array 2192')
              allocate(Tract_axisem(3, NGLLSQUARE * num_abs_boundary_faces), stat = ier)
              if (ier /= 0) call exit_MPI_without_rank('error allocating array 2193')

              open(unit = IIN_veloc_dsm, file = dsmname(1:len_trim(dsmname)) // 'sol_axisem', status = 'old', &
                   action = 'read', form = 'unformatted', iostat = ier)
              write(*, *) 'OPENING ', dsmname(1:len_trim(dsmname)) // 'sol_axisem'

              !! CD CD added this
              if (RECIPROCITY_AND_KH_INTEGRAL) then

                 allocate(Displ_axisem_time(3, NGLLSQUARE * num_abs_boundary_faces, NSTEP), stat = ier)
                 if (ier /= 0) call exit_MPI_without_rank('error allocating array 2194')
                 allocate(Tract_axisem_time(3, NGLLSQUARE * num_abs_boundary_faces, NSTEP), stat = ier)
                 if (ier /= 0) call exit_MPI_without_rank('error allocating array 2195')
                 allocate(Tract_specfem_time(3, NGLLSQUARE * num_abs_boundary_faces, NSTEP), stat = ier)
                 if (ier /= 0) call exit_MPI_without_rank('error allocating array 2196')
                 allocate(Displ_specfem_time(3, NGLLSQUARE * num_abs_boundary_faces, NSTEP), stat = ier)
                 if (ier /= 0) call exit_MPI_without_rank('error allocating array 2197')

                 if (.not.SAVE_RUN_BOUN_FOR_KH_INTEGRAL) then
                    !! We only read Specfem Tract and Displ, and Axisem Displ (Axisem Tract is read in compute_stacey_visco...)
                    !! This is only for KH integral
                    !! The unit numbers are here temporary
                    open(unit = IIN_displ_axisem, file = dsmname(1:len_trim(dsmname)) // 'axisem_displ_for_int_KH', &
                         status = 'old', action = 'read', form = 'unformatted', iostat = ier)
                    
                    open(unit = 237, file = dsmname(1:len_trim(dsmname)) // 'specfem_displ_for_int_KH', &
                         status = 'old', action = 'read', form = 'unformatted', iostat = ier)
                    
                    open(unit = 238, file = dsmname(1:len_trim(dsmname)) // 'specfem_tract_for_int_KH', &
                         status = 'old', action = 'read', form = 'unformatted', iostat = ier)
                    
                    write(*, *) 'OPENING ', dsmname(1:len_trim(dsmname)) // &
                                'axisem_displ_for_int_KH, and the specfem disp and tract'
                 endif
                 
              endif
           end if
        endif

    else
        ! dummy arrays
        allocate(Veloc_dsm_boundary(1, 1, 1, 1), stat = ier)
        if (ier /= 0) call exit_MPI_without_rank('error allocating array 2198')
        allocate(Tract_dsm_boundary(1, 1, 1, 1), stat = ier)
        if (ier /= 0) call exit_MPI_without_rank('error allocating array 2199')
        allocate(Veloc_axisem(1, 1), stat = ier)
        if (ier /= 0) call exit_MPI_without_rank('error allocating array 2200')
        allocate(Tract_axisem(1, 1), stat = ier)
        if (ier /= 0) call exit_MPI_without_rank('error allocating array 2201')
    endif

    !! CD CD add this :
    !! We perform a first run of Specfem to save displacement and tractions of Specfem for the computation of KH integral
    !! The displ, tract, and veloc of Axisem have also to be stored
    if (SAVE_RUN_BOUN_FOR_KH_INTEGRAL) then
        open(unit = 237, file = dsmname(1:len_trim(dsmname)) // 'specfem_displ_for_int_KH', form = 'unformatted')
        open(unit = 238, file = dsmname(1:len_trim(dsmname)) // 'specfem_tract_for_int_KH', form = 'unformatted')
        write(*, *) 'OPENING ', dsmname(1:len_trim(dsmname)) // 'specfem_displ_for_int_KH, and the specfem tract to SAVE IT'
    endif

end subroutine couple_with_injection_setup

!
!-------------------------------------------------------------------------------------------------
!

subroutine couple_with_injection_prepare_boundary()

    use specfem_par
    use specfem_par_coupling
    use specfem_par_elastic, only: ispec_is_elastic
    implicit none
    ! local parameters
    ! timing
    double precision :: tstart, tCPU
    double precision, external :: wtime
    integer :: ier
    !! for FK point for intialization injected wavefield
    real(kind = CUSTOM_REAL) :: Xmin_box, Xmax_box, Ymin_box, Ymax_box, Zmin_box, Zmax_box

    !  initial setup for future FK3D calculations

    if (COUPLE_WITH_INJECTION_TECHNIQUE .and. SIMULATION_TYPE == 1) then

        ! FK boundary
        if (INJECTION_TECHNIQUE_TYPE == INJECTION_TECHNIQUE_IS_FK) then

            ! get MPI starting time for FK
            tstart = wtime()

            ! counts total number of (local) GLL points on absorbing boundary
            call nbound(NSPEC_AB, num_abs_boundary_faces, abs_boundary_ispec, ispec_is_elastic, npt)

            !! compute the bottom midle point of the domain

            !! VM VM dealocate in case of severals runs occurs in inverse_problem program
            if (allocated(nbdglb)) deallocate(nbdglb)
            if (allocated(vx_FK)) deallocate(vx_FK)
            if (allocated(vy_FK)) deallocate(vy_FK)
            if (allocated(vz_FK)) deallocate(vz_FK)
            if (allocated(tx_FK)) deallocate(tx_FK)
            if (allocated(ty_FK)) deallocate(ty_FK)
            if (allocated(tz_FK)) deallocate(tz_FK)
            if (allocated(VX_t)) deallocate(VX_t)
            if (allocated(VY_t)) deallocate(VY_t)
            if (allocated(VZ_t)) deallocate(VZ_t)
            if (allocated(TX_t)) deallocate(TX_t)
            if (allocated(TY_t)) deallocate(TY_t)
            if (allocated(TZ_t)) deallocate(TZ_t)

            !! allocate memory for FK solution
            if (npt > 0) then
                allocate(nbdglb(npt), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2202')
                allocate(vx_FK(npt), vy_FK(npt), vz_FK(npt), tx_FK(npt), ty_FK(npt), tz_FK(npt), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2203')
            else
                allocate(nbdglb(1), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2204')
                allocate(vx_FK(1), vy_FK(1), vz_FK(1), tx_FK(1), ty_FK(1), tz_FK(1), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2205')
            endif

            call FindBoundaryBox(Xmin_box, Xmax_box, Ymin_box, Ymax_box, Zmin_box, Zmax_box)

            call ReadFKModelInput(Xmin_box, Xmax_box, Ymin_box, Ymax_box, Zmin_box)

            ! send FK parameters to others MPI slices
            call bcast_all_singlei(kpsv)
            call bcast_all_singlei(nlayer)
            if (myrank > 0) then
                allocate(al_FK(nlayer), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2206')
                allocate(be_FK(nlayer), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2207')
                allocate(mu_FK(nlayer), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2208')
                allocate(h_FK(nlayer), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2209')
            endif

            call bcast_all_cr(al_FK, nlayer)
            call bcast_all_cr(be_FK, nlayer)
            call bcast_all_cr(mu_FK, nlayer)
            call bcast_all_cr(h_FK, nlayer)
            call bcast_all_singlecr(phi_FK)
            call bcast_all_singlecr(theta_FK)
            call bcast_all_singlecr(fmax_fk)
            call bcast_all_singlecr(td)
            call bcast_all_singlecr(xx0)
            call bcast_all_singlecr(yy0)
            call bcast_all_singlecr(zz0)
            call bcast_all_singlecr(tt0)
            call bcast_all_singlecr(Z_REF_for_FK)
            call bcast_all_singlecr(tmax_fk)
            call bcast_all_singlel(stag)
            call bcast_all_singlei(isw)

            ! converts to rad
            phi_FK = phi_FK * acos(-1.d0)/180.d0
            theta_FK = theta_FK * acos(-1.d0)/180.d0

            if (kpsv == 1) then
                p = sin(theta_FK)/al_FK(nlayer)
            else if (kpsv == 2 .or. kpsv == 3) then
                p = sin(theta_FK)/be_FK(nlayer)
            endif

!            tg = 1.d0/ff0

            if (npt > 0) then

                call find_size_of_working_arrays(deltat, tmax_fk, NF_FOR_STORING, NF_FOR_FFT, NPOW_FOR_INTERP, NP_RESAMP, DF_FK)

                if (myrank == 0) then
                    write(IMAIN, *) 'ALLOCATE SAVED ARRAYS', NF_FOR_STORING
                    write(IMAIN, *)
                    write(IMAIN, *) '  number of frequencies to store = ', NF_FOR_STORING
                    write(IMAIN, *) '  number of frequencies for FFT  = ', NF_FOR_FFT
                    write(IMAIN, *) '  power of 2 for FFT             = ', NPOW_FOR_INTERP
                    write(IMAIN, *) '  resampling rate                = ', NP_RESAMP
                    write(IMAIN, *)
                    write(IMAIN, *) '  new time step for F-K          = ', NP_RESAMP * deltat
                    write(IMAIN, *) '  new time window length         = ', tmax_fk
                    write(IMAIN, *) '  frequency step for F-K         = ', DF_FK
                    write(IMAIN, *)
                    write(IMAIN, *) '  total number of points on boundary = ', npt
                    call flush_IMAIN()
                endif

                ! safety check with number of simulation time steps
                if (NSTEP / 2 > NF_FOR_STORING + NP_RESAMP) then
                    print *, 'Error: FK time window length ', tmax_fk, ' and NF_for_storing ', NF_FOR_STORING
                    print *, '       are too small for chosen simulation length with NSTEP = ', NSTEP
                    print *
                    print *, '       you could use a smaller NSTEP <= ', NF_FOR_STORING * 2
                    print *, '       or'
                    print *, '       increase FK window length larger than ', (NSTEP/2 - NP_RESAMP) * NP_RESAMP * deltat
                    print *, '       to have a NF for storing  larger than ', (NSTEP/2 - NP_RESAMP)
                    stop 'Invalid FK setting'
                endif

                !! arrays for storing FK solution --------------------------------------------

                allocate(VX_t(npt, -NP_RESAMP:NF_FOR_STORING + NP_RESAMP), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2210')
                if (ier /= 0) stop 'error while allocating VX_t'
                VX_t(:,:) = 0._CUSTOM_REAL

                allocate(VY_t(npt, -NP_RESAMP:NF_FOR_STORING + NP_RESAMP), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2211')
                if (ier /= 0) stop 'error while allocating VY_t'
                VY_t(:,:) = 0._CUSTOM_REAL

                allocate(VZ_t(npt, -NP_RESAMP:NF_FOR_STORING + NP_RESAMP), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2212')
                if (ier /= 0) stop 'error while allocating VZ_t'
                VZ_t(:,:) = 0._CUSTOM_REAL

                allocate(TX_t(npt, -NP_RESAMP:NF_FOR_STORING + NP_RESAMP), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2213')
                if (ier /= 0) stop 'error while allocating TX_t'
                TX_t(:,:) = 0._CUSTOM_REAL

                allocate(TY_t(npt, -NP_RESAMP:NF_FOR_STORING + NP_RESAMP), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2214')
                if (ier /= 0) stop 'error while allocating TY_t'
                TY_t(:,:) = 0._CUSTOM_REAL

                allocate(TZ_t(npt, -NP_RESAMP:NF_FOR_STORING + NP_RESAMP), stat = ier)
                if (ier /= 0) call exit_MPI_without_rank('error allocating array 2215')
                if (ier /= 0) stop 'error while allocating TZ_t'
                TZ_t(:,:) = 0._CUSTOM_REAL

                call FK3D(NSPEC_AB, ibool, abs_boundary_ijk, abs_boundary_normal, &
                abs_boundary_ispec, num_abs_boundary_faces, ispec_is_elastic, &
                kpsv, nlayer, nstep, npt, nbdglb, &
                p, phi_FK, xx0, yy0, zz0, td, &
                tt0, al_FK, be_FK, mu_FK, h_FK, deltat, &
                NF_FOR_STORING, NPOW_FOR_FFT, NP_RESAMP, DF_FK)

            endif

            call synchronize_all()

            ! get MPI ending time for FK
            tCPU = wtime() - tstart

            ! user output
            if (myrank == 0) then
                write(IMAIN, '(a35,1x, f20.2, a7)') " Elapsed time for FK computation : ", tCPU, " sec. "
                write(IMAIN, *)
                call flush_IMAIN()
            endif

            deallocate(al_FK, be_FK, mu_FK, h_FK)
        endif
    endif

    ! * end of initial setup for future FK3D calculations *

end subroutine couple_with_injection_prepare_boundary

!
!-------------------------------------------------------------------------------------------------
!

!! count the number of point in the mesh partition boundary : npt

subroutine nbound(NSPEC_AB, num_abs_boundary_faces, abs_boundary_ispec, ispec_is_elastic, npt)

    use constants

    implicit none

    integer, intent(inout) :: npt
    integer, intent(in) :: NSPEC_AB, num_abs_boundary_faces
    ! elastic domain flag
    logical, dimension(NSPEC_AB), intent(in) :: ispec_is_elastic
    ! absorbing boundary surface
    integer, dimension(num_abs_boundary_faces), intent(in) :: abs_boundary_ispec
    ! local parameters
    integer :: ispec, iface

    npt = 0
    do iface = 1, num_abs_boundary_faces
        ispec = abs_boundary_ispec(iface)
        if (ispec_is_elastic(ispec)) then
            ! reference GLL points on boundary face
            npt = npt + NGLLSQUARE
        endif
    enddo

end subroutine nbound

!
!-------------------------------------------------------------------------------------------------
!

subroutine FK3D(NSPEC_AB, ibool, abs_boundary_ijk, abs_boundary_normal, &
    abs_boundary_ispec, num_abs_boundary_faces, ispec_is_elastic, kpsv, nlayer, nstep, npt, nbdglb, &
    p, phi, xx0, yy0, zz0, td, tt0, al_FK, be_FK, mu_FK, h_FK, deltat, &
    NF_FOR_STORING, NPOW_FOR_FFT, NP_RESAMP, DF_FK)

    use constants

    use specfem_par, only: xstore, ystore, zstore, kappastore, mustore, rhostore
    use specfem_par_coupling, only: xx, yy, zz, xi1, xim, bdlambdamu, &
    nmx, nmy, nmz, Z_REF_for_FK, isw
    !BP
    use specfem_par, only: sizeprocs

    implicit none

    integer :: NSPEC_AB, kpsv, nlayer, npt, nstep, ipt
    integer :: NF_FOR_STORING, NPOW_FOR_FFT, NP_RESAMP
    ! global index
    integer, dimension(NGLLX, NGLLY, NGLLZ, NSPEC_AB) :: ibool
    integer, dimension(npt) :: nbdglb

    ! source
    real(kind = CUSTOM_REAL) :: p, phi, xx0, yy0, zz0, tt0
    real(kind = CUSTOM_REAL) :: DF_FK

    ! model
    real(kind = CUSTOM_REAL), dimension(nlayer) :: al_FK, be_FK, mu_FK, h_FK

    real(kind = CUSTOM_REAL) :: rhotmp, kappatmp, mutmp, xi, deltat, td
    logical, dimension(NSPEC_AB) :: ispec_is_elastic

    ! absorbing boundary surface
    integer :: num_abs_boundary_faces
    integer :: abs_boundary_ijk(3, NGLLSQUARE, num_abs_boundary_faces)
    integer :: abs_boundary_ispec(num_abs_boundary_faces)
    real(kind = CUSTOM_REAL), dimension(3, NGLLSQUARE, num_abs_boundary_faces) :: abs_boundary_normal

    ! local parameters
    integer :: ispec, iglob, i, j, k, iface, igll, ier
    integer, dimension(num_abs_boundary_faces * NGLLSQUARE) :: ipt_elastic

    !BP 
    integer, dimension(1) :: npt_mess
    integer, dimension(sizeprocs) :: npt_table

    ! absorbs absorbing-boundary surface using Stacey condition (Clayton and Enquist)
    if (npt > 0) then
        allocate(xx(npt), yy(npt), zz(npt), xi1(npt), xim(npt), bdlambdamu(npt), nmx(npt), nmy(npt), nmz(npt), stat = ier)
        if (ier /= 0) call exit_MPI_without_rank('error allocating array 2216')
    else
        allocate(xx(1), yy(1), zz(1), xi1(1), xim(1), bdlambdamu(1), nmx(1), nmy(1), nmz(1), stat = ier)
        if (ier /= 0) call exit_MPI_without_rank('error allocating array 2217')
    endif

    nbdglb(:) = 0
    ipt = 0

    do iface = 1, num_abs_boundary_faces
        ispec = abs_boundary_ispec(iface)
        if (ispec_is_elastic(ispec)) then
            ! reference GLL points on boundary face
            do igll = 1, NGLLSQUARE

                ! gets local indices for GLL point
                i = abs_boundary_ijk(1, igll, iface)
                j = abs_boundary_ijk(2, igll, iface)
                k = abs_boundary_ijk(3, igll, iface)

                iglob = ibool(i, j, k, ispec)
                ipt = ipt + 1
                nbdglb(ipt) = iglob
                
                ipt_elastic((iface-1)*NGLLSQUARE + igll) = ipt !! BP Store ipt for GPU computation

                xx(ipt) = xstore(iglob)
                yy(ipt) = ystore(iglob)
                zz(ipt) = zstore(iglob) - Z_REF_for_FK !! VM VM put z in FK system of coordinate

                nmx(ipt) = abs_boundary_normal(1, igll, iface)
                nmy(ipt) = abs_boundary_normal(2, igll, iface)
                nmz(ipt) = abs_boundary_normal(3, igll, iface)

                rhotmp = rhostore(i, j, k, ispec)
                kappatmp = kappastore(i, j, k, ispec)
                mutmp = mustore(i, j, k, ispec)

                xi = mutmp/(kappatmp + 4.0 * mutmp/3.0)
                xi1(ipt) = 1.0 - 2.0 * xi
                xim(ipt) = (1.0 - xi) * mutmp
                bdlambdamu(ipt) = (3.0 * kappatmp - 2.0 * mutmp)/(6.0 * kappatmp + 2.0 * mutmp)

            enddo

        endif ! ispec_is_elastic
        !! may not be necessary..
!        else
!            do igll = 1, NGLLSQUARE
!                ipt_elastic((iface-1)*NGLLSQUARE + igll) = 0
!            enddo
!        endif
    enddo


    !BP
    npt_mess(1) = npt
    call gather_all_all_i(npt_mess, 1, npt_table, 1, sizeprocs)

    !print *,'Moi processus ', myrank,' parmis ',sizeprocs,' ai reçu: ',npt_table(1:sizeprocs)

    
    call FK(al_FK, be_FK, mu_FK, h_FK, nlayer, td, p, phi, xx0, yy0, zz0, tt0, deltat, nstep, npt, &
            kpsv, NF_FOR_STORING, NPOW_FOR_FFT, NP_RESAMP, DF_FK, npt_table, isw)
    

    deallocate(xx, yy, zz, xi1, xim, bdlambdamu, nmx, nmy, nmz)

end subroutine FK3D

!
!-------------------------------------------------------------------------------------------------
!

subroutine FK(al, be, mu, h, nlayer, Td, p, phi, x0, y0, z0, t0, dt, npts, np, &
    kpsv, NF_FOR_STORING, NPOW_FOR_FFT, NP_RESAMP, DF_FK, npt_table, isw)

    use constants
    use omp_lib
    use FFTW
    use shared_parameters, only: USE_FFTW_INV
    use specfem_par, only: myrank, sizeprocs
    use specfem_par_coupling, only: xx, yy, zz, xi1, xim, bdlambdamu, &
    nmx, nmy, nmz, NPTS_STORED, NPTS_INTERP!, vx_t, vy_t, vz_t, Tx_t, Ty_t, tz_t

    implicit none

    integer, parameter :: CUSTOM_CMPLX = 8
    real(kind = CUSTOM_REAL), parameter :: zign_neg = -1.
    ! input and output
    integer, intent(in) :: nlayer, np, npts, kpsv
    integer :: NF_FOR_STORING, NPOW_FOR_FFT, NP_RESAMP

    ! model
    real(kind = CUSTOM_REAL), dimension(nlayer), intent(in) :: al(nlayer), be(nlayer), mu(nlayer), H(nlayer)
    !BP! Define cos(phi) and sin(phi) as variables in order to reduce computing time
    real(kind = CUSTOM_REAL) :: cosPhi, sinPhi, cos2Phi, sin2Phi

    ! source
    real(kind = CUSTOM_REAL), intent(in) :: dt, p, phi, x0, y0, z0, Td, t0, DF_FK
    integer, intent(in) :: isw

    real(kind = CUSTOM_REAL), dimension(:), allocatable :: fvec, dtmp
    complex(kind = CUSTOM_CMPLX), dimension(:,:), allocatable :: coeff
    !!BP
    !!Redefine allocation of field and field_f according to the usage of FFTW or not 
    complex(kind = CUSTOM_CMPLX), dimension(:,:), allocatable :: field_f
    real(kind = CUSTOM_REAL), dimension(:,:), allocatable :: field
    complex(kind = C_DOUBLE_COMPLEX), dimension(:,:), allocatable :: field_f_w
    real(kind = C_DOUBLE), dimension(:,:), allocatable :: field_w

    complex(kind = CUSTOM_CMPLX), dimension(:), allocatable :: tmp_f1, tmp_f2, tmp_f3
    real(kind = CUSTOM_REAL), dimension(:), allocatable :: tmp_t1, tmp_t2, tmp_t3, tmp_it1

    complex(kind = CUSTOM_CMPLX) :: C_3, stf_coeff, a, b, c, d, delta_mat
    !BP
    complex(kind = CUSTOM_CMPLX), dimension(:,:), allocatable :: N_mat
    complex(kind = CUSTOM_CMPLX), dimension(:,:,:), allocatable :: N_mat_layers
    complex(kind = CUSTOM_CMPLX), dimension(:,:,:), allocatable :: N_mat_bottom, N_mat_temp
    complex(kind = CUSTOM_CMPLX), dimension(nlayer) :: eta_al, eta_be
    real(kind = CUSTOM_REAL), dimension(nlayer) :: gamma0, gamma1
    complex(kind = CUSTOM_CMPLX), dimension(:), allocatable :: nu_al, nu_be
    integer, dimension(sizeprocs) :: npt_table
    real(kind = CUSTOM_REAL) :: sum_H
    integer :: ilayer, NmatDim
    real(kind = CUSTOM_REAL) :: hh
    real(kind = CUSTOM_REAL) :: ht_bottom, ht_temp

    !BP
    !$  integer                                                    :: npt_min, npt_max, OMP_DYN_SCHEDULE, nthreads
    
    real(kind = CUSTOM_REAL) :: epsil, dt_fk
    real(kind = CUSTOM_REAL) :: df, om, tdelay, eta_p, eta_s, fmax, C_1
    integer :: npow, npts2, nf, nf2, nn, ii, ip, i, nvar 
    integer :: npoints2
    integer :: ier
    logical :: comp_stress, pout

    !! DK DK here is the hardwired maximum size of the array
    !! DK DK Aug 2016: if this routine is called many times (for different mesh points at which the SEM is coupled with FK)
    !! DK DK Aug 2016: this should be moved to the calling program and precomputed once and for all
    real(kind = CUSTOM_REAL) :: mpow(30)

    integer :: control = 0

    epsil = 1.0e-7
    comp_stress = .true.
    ! BP
    if(kpsv == 3) then
        nvar = 3
        NmatDim = 2
    else
        nvar = 5
        NmatDim = 4
    endif
    pout = .false.

    fmax = 1/(2 * dt) ! Nyquist frequency of specfem time serie

    !! new way to do time domain resampling
    df = df_fk
    nf2 = NF_FOR_STORING + 1 ! number of positive frequency sample points
    nf = 2 * NF_FOR_STORING ! number of total frequencies after symetrisation
    npts2 = nf ! number of samples in time serie

    !! VM VM recompute new values for new way to do
    npow = ceiling(log(npts2 * 1.)/log(2.))
    npts2 = 2**npow
    NPOW_FOR_FFT = npow

    dt_fk = 1./(df * (npts2 - 1))

    !! BP! Compute once and for all cos(phi) and sin(phi)
    cosPhi = cos(phi)
    sinPhi = sin(phi)
    cos2Phi = cos(2.0 * phi)
    sin2Phi = sin(2.0 * phi)
    

    !! number of points for resmpled vector
    npoints2 = NP_RESAMP * (npts2 - 1) + 1

    if (myrank == 0) then
        write(IMAIN, *)
        write(IMAIN, *) 'Entering the FK synthetics program:'
        write(IMAIN, *) '  Number of points used for FFT            = ', npts2
        write(IMAIN, *) '  Number of samples stored for FK solution = ', NF_FOR_STORING
        write(IMAIN, *) '  Total time length used for FK            = ', t0 + (npts2 - 1) * dt_fk
        write(IMAIN, *) '  FK time step       = ', dt_fk
        write(IMAIN, *) '  FK frequency step  = ', df
        if  (USE_FFTW_INV) then
            write(IMAIN, *) '  FFT computed using FFTW'
        else
            write(IMAIN, *) '  FFT computed using classic Specfem3D FFT'
            write(IMAIN, *) '  power of 2 for FFT = ', npow
        endif
        call flush_IMAIN()
    endif

    !! check if dt_fk is compatible with dt_specfem
    !!
    !!
    !!

    allocate(fvec(nf2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2218')
    fvec = 0.
    do ii = 1, nf2
        fvec(ii) = (ii - 1) * df
    enddo

    allocate(coeff(2, nf2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2219')
    if (ier /= 0) stop 'error while allocating'

    !!BP
    if (USE_FFTW_INV) then
        allocate(field_f_w(nf, nvar), stat = ier)
    else
        allocate(field_f(nf, nvar), stat = ier)
    endif
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2220')
    if (ier /= 0) stop 'error while allocating'

    if (USE_FFTW_INV) then
        allocate(field_w(npts2, nvar), dtmp(npts), stat = ier)
    else
        allocate(field(npts2, nvar), dtmp(npts), stat = ier)
    endif
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2221')
    if (ier /= 0) stop 'error while allocating'

    !BP
    allocate(N_mat(NmatDim,NmatDim), stat=ier)
    if (ier /= 0) stop 'error while allocating N_mat'

    allocate(N_mat_layers(NmatDim, NmatDim, nlayer * nf2), stat = ier)
    if (ier /= 0) stop 'error while allocating N_mat_layers'

    allocate(N_mat_bottom(NmatDim, NmatDim, nf2), stat = ier)
    if (ier /= 0) stop 'error while allocating N_mat_bottom'

    allocate(N_mat_temp(NmatDim, NmatDim, nf2), stat = ier)
    if (ier /= 0) stop 'error while allocating N_mat_temp'
    
    allocate(nu_al(nlayer * nf2), stat = ier)
    if (ier /= 0) stop 'error while allocating nu_al'

    allocate(nu_be(nlayer * nf2), stat = ier)
    if (ier /= 0) stop 'error while allocating nu_be'
    !BP

    !! allocate debug vectors
    allocate(tmp_f1(npts2), tmp_f2(npts2), tmp_f3(npts2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2222')

    if (ier /= 0) stop 'error while allocating'
    allocate(tmp_t1(npts2), tmp_t2(npts2), tmp_t3(npts2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2223')

    if (ier /= 0) stop 'error while allocating'
    allocate(tmp_it1(npoints2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2224')

    NPTS_STORED = npts2
    NPTS_INTERP = npoints2

    tmp_t1(:) = 0.
    tmp_t2(:) = 0.
    tmp_t3(:) = 0.

    tmp_f1(:) = (0., 0.)
    tmp_f2(:) = (0., 0.)
    tmp_f3(:) = (0., 0.)

    nn = int(-t0/dt) ! what if this is not an integer number?

    !! DK DK Aug 2016: if this routine is called many times (for different mesh points at which the SEM is coupled with FK)
    !! DK DK Aug 2016: this should be moved to the calling program and precomputed once and for all
    do i = 1, npow
        mpow(i) = 2**(npow - i)
    enddo

    if (myrank == 0) then
        write(IMAIN, *)
        write(IMAIN, *) 'starting from ', nn, ' points before time 0'
        call flush_IMAIN()
    endif

    !BP
    if (USE_FFTW_INV) then
        call init_FFTW(npts2, nf)
    endif

    !$ nthreads = omp_get_max_threads()
    !$ print *,'nthreads = ', nthreads

    !$ npt_min = minval(npt_table, dim = 1, mask = (npt_table .ne. 0))
    !$ npt_max = maxval(npt_table, dim = 1)

    !$ if (np .le. npt_min) then
    !$     OMP_DYN_SCHEDULE = np
    !$ else if (np == npt_max) then
    !$     OMP_DYN_SCHEDULE = ceiling(dble(np) / dble(nthreads))
    !$ else
    !$     if(nthreads == 2) then
    !$       OMP_DYN_SCHEDULE = ceiling(dble(np) / dble(nthreads))
    !$     else
    !$       OMP_DYN_SCHEDULE = ceiling(dble(np) / ceiling(dble(nthreads) / 2.0))
    !$     endif
    !$ end if

    !BP
    N_mat_layers(:,:,:) = (0.0, 0.0)
    N_mat_bottom(:,:,:) = (0.0, 0.0)
    N_mat_temp(:,:,:) = (0.0, 0.0)
    ht_bottom = 1.d+30
    ht_temp = 0.d+0

    if(kpsv == 3) then
        call compute_Sh_propagator(al, be, mu, H, nlayer, nf2, fvec, p, eta_al, eta_be, nu_be, N_mat_layers)
    else
        call compute_N_rayleigh(al, be, mu, H, nlayer, nf2, fvec, p, eta_al, eta_be, nu_al, nu_be, gamma0, gamma1, N_mat_layers)
    endif

    !compute sum(H(1:nlayer-1)) once and for all
    sum_H = sum(H(1:nlayer - 1))

    if (kpsv == 1) then
        ! P-wave

        ! for C_3=i sin(inc) (u=[sin(inc), cos(inc)])
        C_3 = cmplx(0, 1.) * p * al(nlayer) ! amp. of incoming P in the bot. layer
        eta_p = sqrt(1./al(nlayer)**2 - p**2) ! vertical slowness for lower layer

        if (myrank == 0) write(IMAIN, *) 'Incoming P : C_3,  p, eta = ', C_3, p, eta_p

        N_mat(:,:) = (0.0, 0.0)

        ! find out the wave coefficients in the bottom layer for all freqs -------------------------------
        !$OMP PARALLEL DO SCHEDULE(DYNAMIC, OMP_DYN_SCHEDULE) DEFAULT(NONE) SHARED(N_mat_layers, mu, H, nlayer, p, eta_al, eta_be, &
        !$OMP nu_al, nu_be, gamma0, gamma1, coeff, C_3, nf2, sum_H) FIRSTPRIVATE(a, b, c, d, delta_mat, N_mat, ilayer, hh)
        do ii = 1, nf2
            ! propagation matrix
            call get_ilayer(ilayer, hh, sum_H, H, nlayer)
            call compute_last_N_rayleigh(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, nlayer, &
            p, eta_al, eta_be, nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer), gamma0, gamma1,&
            hh, ilayer)

            a = N_mat(3, 2); b = N_mat(3, 4); c = N_mat(4, 2); d = N_mat(4, 4)
            delta_mat = a * d - b * c
            coeff(1, ii) = -(d * N_mat(3, 3) - b * N_mat(4, 3))/delta_mat * C_3
            coeff(2, ii) = -(-c * N_mat(3, 3) + a * N_mat(4, 3))/delta_mat * C_3    
                        
        enddo
        !$OMP END PARALLEL DO
        
        ! loop over all data points -------------------------------------------------
        !$OMP PARALLEL DO SCHEDULE(DYNAMIC, OMP_DYN_SCHEDULE) DEFAULT(NONE) SHARED(N_mat_layers, mu, H, nlayer, p, eta_al, eta_be,&
        !$OMP nu_al, nu_be, gamma0, gamma1, C_3, eta_p, comp_stress, xi1, xim, cosPhi, sinPhi, nmx, nmy, nmz, &
        !$OMP npow, mpow, dt, x0, y0, z0, td, fvec, zz, coeff, nf2, nvar, nn, xx, yy, npts2, NF_FOR_STORING, nf, npts, &
        !$OMP myrank, bdlambdamu, np, USE_FFTW_INV, sum_H) FIRSTPRIVATE(delta_mat, N_mat, om, tdelay, field_f,field_f_w, &
        !$OMP stf_coeff, field, field_w, tmp_t1, tmp_t2, dtmp, ii, ilayer, hh, ht_bottom, N_mat_bottom, ht_temp, N_mat_temp)
        do ip = 1, np ! maybe this can be run faster by shifting t for diff. x of fixed z

            !BP
            if (zz(ip) > sum_H) then
                write(*, *) ' FK error '
                write(*, *) ' Z point is located in the air above the surface rather than in the solid!'
                write(*, *) ' current z :', zz(ip), ' max z allowed : ', sum_H
                stop
            else if (zz(ip) <= 0) then
                if (USE_FFTW_INV) then

                    field_f_w = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_p * (0 - z0)

                    if (ht_bottom /= zz(ip)) then
                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            !! zz(ip) is the height of point with respect to the lower layer
                            !BP
                            call compute_lower_half_space_N_rayleigh(N_mat_layers(:,:, ii * nlayer), N_mat, zz(ip), nlayer, &
                            nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer))

                            N_mat_bottom(:,:, ii) = N_mat(:,:)

                            call compute_field_f_w_P(field_f_w, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, &
                            nf2, xi1(ip), xim(ip))

                        enddo
                        ht_bottom = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            !! zz(ip) is the height of point with respect to the lower layer
                            !BP
                            N_mat(:,:) = N_mat_bottom(:,:, ii)

                            call compute_field_f_w_P(field_f_w, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, &
                            nf2, xi1(ip), xim(ip))

                        enddo
                    endif

                    call compute_FFTW_inv(field_w, field_f_w, nf, nf2, npts, npts2, nvar, nn)

                     call store_undersampled_velocity_traction_FK_w(field_w, cosPhi, sinPhi, npts2, nvar, dt, nmx(ip), nmy(ip),&
                    nmz(ip), bdlambdamu(ip), NF_FOR_STORING, ip)

                else

                    field_f = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_p * (0 - z0)

                    if (ht_bottom /= zz(ip)) then

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)
                  
                            !! zz(ip) is the height of point with respect to the lower layer
                            !BP
                            call compute_lower_half_space_N_rayleigh(N_mat_layers(:,:, ii * nlayer), N_mat, zz(ip), nlayer, &
                            nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer))

                            N_mat_bottom(:,:, ii) = N_mat(:,:)
                            
                            call compute_field_f_P(field_f, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, &
                            xi1(ip), xim(ip))

                        enddo

                        ht_bottom = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            !! zz(ip) is the height of point with respect to the lower layer
                            !BP
                            N_mat(:,:) = N_mat_bottom(:,:, ii)
                            
                            call compute_field_f_P(field_f, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, &
                            xi1(ip), xim(ip))

                        enddo

                    endif
                    
                    call compute_FFT_inv(field, field_f, nf, nf2, npts, npts2, nvar, nn, npow, zign_neg, dt, mpow)
                    
                    call store_undersampled_velocity_traction_FK(field, cosPhi, sinPhi, npts2, nvar, nmx(ip), nmy(ip), nmz(ip),&
                    bdlambdamu(ip), NF_FOR_STORING, ip)

                endif

            else
                if (USE_FFTW_INV) then

                    field_f_w = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_p * (0 - z0)
                    call get_ilayer(ilayer, hh, zz(ip), H, nlayer)

                    if (ht_temp /= zz(ip)) then
                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            !! zz(ip) is the height of point with respect to the lower layer
                            !BP
                            call compute_last_N_rayleigh(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, nlayer,p,&
                            eta_al, eta_be, nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer), &
                            gamma0, gamma1, hh, ilayer)

                            N_mat_temp(:,:, ii) = N_mat(:,:)

                            call compute_field_f_w_P(field_f_w, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, &
                            nf2, xi1(ip), xim(ip))

                        enddo
                        ht_temp = zz(ip)

                    else
                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            !! zz(ip) is the height of point with respect to the lower layer
                            !BP
                            N_mat(:,:) = N_mat_temp(:,:, ii)

                            call compute_field_f_w_P(field_f_w, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, &
                            nf2, xi1(ip), xim(ip))

                        enddo

                    endif

                    call compute_FFTW_inv(field_w, field_f_w, nf, nf2, npts, npts2, nvar, nn)

                    call store_undersampled_velocity_traction_FK_w(field_w, cosPhi, sinPhi, npts2, nvar, dt, nmx(ip), nmy(ip),&
                    nmz(ip), bdlambdamu(ip), NF_FOR_STORING, ip)

                else

                    field_f = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_p * (0 - z0)
                    call get_ilayer(ilayer, hh, zz(ip), H, nlayer)

                    if (ht_temp /= zz(ip)) then

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            !! zz(ip) is the height of point with respect to the lower layer
                            !BP
                            call compute_last_N_rayleigh(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, nlayer,p,&
                            eta_al, eta_be, nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer), &
                            gamma0, gamma1, hh, ilayer)

                            N_mat_temp(:,:, ii) = N_mat(:,:)

                           call compute_field_f_P(field_f, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, &
                            xi1(ip), xim(ip))
                            
                        enddo

                        ht_temp = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            !! zz(ip) is the height of point with respect to the lower layer
                            !BP
                            N_mat(:,:) = N_mat_temp(:,:, ii)
                          
                            call compute_field_f_P(field_f, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, &
                            xi1(ip), xim(ip))

                        enddo

                    endif

                    call compute_FFT_inv(field, field_f, nf, nf2, npts, npts2, nvar, nn, npow, zign_neg, dt, mpow)

                    call store_undersampled_velocity_traction_FK(field, cosPhi, sinPhi, npts2, nvar, nmx(ip), nmy(ip), nmz(ip),&
                    bdlambdamu(ip), NF_FOR_STORING, ip)

                endif

            endif

            ! user output
            if (myrank == 0 .and. np > 1000) then
                if (mod(ip, (np/10)) == 0) then
                    write(IMAIN, *) '  done ', ip/(np/10) * 10, '% points out of ', np
                    call flush_IMAIN()
                endif
            endif
        enddo
        !$OMP END PARALLEL DO

    else if (kpsv == 2) then
        ! SV-wave

        ! for C_2= sin(inc) (u=[cos(inc), sin(inc)])
        C_1 = p * be(nlayer) ! amp. of incoming S in the bot. layer
        eta_s = sqrt(1./be(nlayer)**2 - p**2) ! vertical slowness for lower layer

        if (myrank == 0) write(IMAIN, *) 'Incoming S :  C_1,  p, eta = ', C_1, p, eta_s

        N_mat(:,:) = (0.0, 0.0)

        ! find out the wave coefficients in the bottom layer for all freqs
        !$OMP PARALLEL DO SCHEDULE(DYNAMIC, OMP_DYN_SCHEDULE) DEFAULT(NONE) SHARED(N_mat_layers, mu, H, nlayer, p, eta_al, eta_be, &
        !$OMP nu_al, nu_be, gamma0, gamma1, coeff, C_1, nf2, sum_H) FIRSTPRIVATE(a, b, c, d, delta_mat, N_mat, ilayer, hh)
        do ii = 1, nf2
            ! propagation matrix
            !if (ii == nf2) pout = .true.
            !BP
            call get_ilayer(ilayer, hh, sum_H, H, nlayer)
            call compute_last_N_rayleigh(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, nlayer, &
            p, eta_al, eta_be, nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer), gamma0, gamma1, &
            hh, ilayer)

            a = N_mat(3, 2); b = N_mat(3, 4); c = N_mat(4, 2); d = N_mat(4, 4)
            delta_mat = a * d - b * c
            coeff(1, ii) = -(d * N_mat(3, 1) - b * N_mat(4, 1))/delta_mat * C_1
            coeff(2, ii) = -(-c * N_mat(3, 1) + a * N_mat(4, 1))/delta_mat * C_1
        enddo
        !$OMP END PARALLEL DO

        ! loop over all data points
        !$OMP PARALLEL DO SCHEDULE(DYNAMIC, OMP_DYN_SCHEDULE) DEFAULT(NONE) SHARED(N_mat_layers, mu, H, nlayer, p, eta_al, eta_be, &
        !$OMP nu_al, nu_be, gamma0, gamma1, C_1, eta_s, comp_stress, xi1, xim, cosPhi, sinPhi, nmx, nmy,nmz,npow,&
        !$OMP mpow, dt, x0, y0, z0, td, fvec, zz, coeff, nf2, nvar, nn, xx, yy, npts2, NF_FOR_STORING, nf, npts, &
        !$OMP myrank, bdlambdamu, np, USE_FFTW_INV, sum_H) FIRSTPRIVATE(delta_mat, N_mat, om, tdelay, field_f,field_f_w,stf_coeff, &
        !$OMP field, field_w, dtmp, tmp_t1, tmp_t2, ii, ilayer, hh, ht_bottom, N_mat_bottom, ht_temp, N_mat_temp)
        do ip = 1, np ! maybe this can be run faster by shifting t for diff. x of fixed z


            !BP
            if (zz(ip) > sum_H) then
                write(*, *) ' FK error '
                write(*, *) ' Z point is located in the air above the surface rather than in the solid!'
                write(*, *) ' current z :', zz(ip), ' max z allowed : ', sum_H
                stop
            else if (zz(ip) <= 0) then
                if (USE_FFTW_INV) then
                    field_f_w = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_s * (0 - z0)

                    if (ht_bottom /= zz(ip)) then
                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            call compute_lower_half_space_N_rayleigh(N_mat_layers(:,:, ii * nlayer), N_mat, zz(ip), nlayer, &
                            nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer))

                            N_mat_bottom(:,:, ii) = N_mat(:,:)

                            call compute_field_f_w_Sv(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om,p,nf2,&
                            xi1(ip), xim(ip))

                        enddo

                        ht_bottom = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            N_mat(:,:) = N_mat_bottom(:,:, ii)

                            call compute_field_f_w_Sv(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om,p,nf2,&
                            xi1(ip), xim(ip))

                        enddo

                    endif

                    call compute_FFTW_inv(field_w, field_f_w, nf, nf2, npts, npts2, nvar, nn)

                    call store_undersampled_velocity_traction_FK_w(field_w, cosPhi, sinPhi, npts2, nvar, dt, nmx(ip), nmy(ip),&
                    nmz(ip), bdlambdamu(ip), NF_FOR_STORING, ip)

                else
                    field_f = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_s * (0 - z0)

                    if (ht_bottom /= zz(ip)) then

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            call compute_lower_half_space_N_rayleigh(N_mat_layers(:,:, ii * nlayer), N_mat, zz(ip), nlayer, &
                            nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer))

                            N_mat_bottom(:,:, ii) = N_mat(:,:)

                            call compute_field_f_Sv(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2,&
                            xi1(ip), xim(ip))

                        enddo

                        ht_bottom = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            N_mat(:,:) = N_mat_bottom(:,:, ii)

                            call compute_field_f_Sv(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2,&
                            xi1(ip), xim(ip))

                        enddo

                    endif

                    call compute_FFT_inv(field, field_f, nf, nf2, npts, npts2, nvar, nn, npow, zign_neg, dt, mpow)

                    call store_undersampled_velocity_traction_FK(field, cosPhi, sinPhi, npts2, nvar, nmx(ip), nmy(ip), nmz(ip),&
                    bdlambdamu(ip), NF_FOR_STORING, ip)

                endif

            else

                if (USE_FFTW_INV) then
                    field_f_w = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_s * (0 - z0)
                    call get_ilayer(ilayer, hh, zz(ip), H, nlayer)

                    if (ht_temp /= zz(ip)) then

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            call compute_last_N_rayleigh(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, nlayer,p,&
                            eta_al, eta_be, nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer), & 
                            gamma0, gamma1, hh, ilayer)

                            N_mat_temp(:,:, ii) = N_mat(:,:)

                            call compute_field_f_w_Sv(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om,p,nf2,&
                            xi1(ip), xim(ip))

                        enddo

                        ht_temp = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            N_mat(:,:) = N_mat_temp(:,:, ii)

                            call compute_field_f_w_Sv(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om,p,nf2,&
                            xi1(ip), xim(ip))

                        enddo


                    endif

                    call compute_FFTW_inv(field_w, field_f_w, nf, nf2, npts, npts2, nvar, nn)

                    call store_undersampled_velocity_traction_FK_w(field_w, cosPhi, sinPhi, npts2, nvar, dt, nmx(ip), nmy(ip),&
                    nmz(ip), bdlambdamu(ip), NF_FOR_STORING, ip)

                else
                    field_f = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_s * (0 - z0)
                    call get_ilayer(ilayer, hh, zz(ip), H, nlayer)

                    if (ht_temp /= zz(ip)) then

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            call compute_last_N_rayleigh(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, nlayer,p,&
                            eta_al, eta_be, nu_al(1 + (ii - 1) * nlayer:ii * nlayer), nu_be(1 + (ii - 1) * nlayer:ii * nlayer), &
                            gamma0, gamma1, hh, ilayer)

                            N_mat_temp(:,:, ii) = N_mat(:,:)

                            call compute_field_f_Sv(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2,&
                            xi1(ip), xim(ip))

                        enddo

                        ht_temp = zz(ip)

                    else
                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            N_mat(:,:) = N_mat_temp(:,:, ii)

                            call compute_field_f_Sv(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2,&
                            xi1(ip), xim(ip))

                        enddo

                    endif

                    call compute_FFT_inv(field, field_f, nf, nf2, npts, npts2, nvar, nn, npow, zign_neg, dt, mpow)

                    call store_undersampled_velocity_traction_FK(field, cosPhi, sinPhi, npts2, nvar, nmx(ip), nmy(ip), nmz(ip),&
                    bdlambdamu(ip), NF_FOR_STORING, ip)

                endif

            endif

        enddo
        !$OMP END PARALLEL DO
    else if(kpsv == 3) then
        ! SH-wave

        ! for C_2= sin(inc) (u=[cos(inc), sin(inc)])
        C_1 = 1.0 ! amp. of incoming S in the bot. layer
        eta_s = sqrt(1./be(nlayer)**2 - p**2) ! vertical slowness for lower layer

        if (myrank == 0) write(IMAIN, *) 'Incoming Sh :  C_1,  p, eta = ', C_1, p, eta_s

        N_mat(:,:) = (0.0, 0.0)

        ! find out the wave coefficients in the bottom layer for all freqs
        !$OMP PARALLEL DO SCHEDULE(DYNAMIC, OMP_DYN_SCHEDULE) DEFAULT(NONE) SHARED(N_mat_layers, mu, H, nlayer, p, eta_al, eta_be, &
        !$OMP nu_al, nu_be, gamma0, gamma1, coeff, C_1, nf2, sum_H) FIRSTPRIVATE(N_mat, ilayer, hh)
        do ii = 1, nf2
            ! propagation matrix
            !if (ii == nf2) pout = .true.
            !BP
            if(myrank == 0 .and. ii< 3)then
                control = 1
            else
                control = 0
            endif
            
            call get_ilayer(ilayer, hh, sum_H, H, nlayer)
            call compute_last_Sh_propagator(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, nlayer, &
            p, eta_al, eta_be, nu_be(1 + (ii - 1) * nlayer:ii * nlayer), hh, ilayer, control)
            
            coeff(1, ii) = -C_1 * N_mat(2,1) / N_mat(2,2)
        enddo
        control = 0
        !$OMP END PARALLEL DO

        ! loop over all data points
        !$OMP PARALLEL DO SCHEDULE(DYNAMIC, OMP_DYN_SCHEDULE) DEFAULT(NONE) SHARED(N_mat_layers, mu, H, nlayer, p, eta_al, eta_be, &
        !$OMP nu_al, nu_be, gamma0, gamma1, C_1, eta_s, comp_stress, xi1, xim, cosPhi, sinPhi, nmx, nmy,nmz,npow,&
        !$OMP mpow, dt, x0, y0, z0, td, isw, fvec, zz, coeff, nf2, nvar, nn, xx, yy, npts2, NF_FOR_STORING, nf, npts, &
        !$OMP myrank, bdlambdamu, np, USE_FFTW_INV, sum_H) FIRSTPRIVATE(delta_mat, N_mat, om, tdelay, field_f,field_f_w,stf_coeff, &
        !$OMP field, field_w, dtmp, tmp_t1, tmp_t2, ii, ilayer, hh, ht_bottom, N_mat_bottom, ht_temp, N_mat_temp)
        do ip = 1, np ! maybe this can be run faster by shifting t for diff. x of fixed z


            !BP
            if (zz(ip) > sum_H) then
                write(*, *) ' FK error '
                write(*, *) ' Z point is located in the air above the surface rather than in the solid!'
                write(*, *) ' current z :', zz(ip), ' max z allowed : ', sum_H
                stop
            else if (zz(ip) <= 0) then
                if (USE_FFTW_INV) then
                    field_f_w = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_s * (0 - z0)

                    if (ht_bottom /= zz(ip)) then
                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            call compute_lower_half_space_Sh_propagator(N_mat_layers(:,:, ii * nlayer), N_mat, zz(ip), nlayer, &
                            nu_be(1 + (ii - 1) * nlayer:ii * nlayer))

                            N_mat_bottom(:,:, ii) = N_mat(:,:)

                            call compute_field_f_w_Sh(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om,&
                            p, nf2, mu(nlayer))

                        enddo

                        ht_bottom = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            N_mat(:,:) = N_mat_bottom(:,:, ii)

                            call compute_field_f_w_Sh(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, &
                            p, nf2, mu(nlayer))

                        enddo

                    endif

                    call compute_FFTW_inv(field_w, field_f_w, nf, nf2, npts, npts2, nvar, nn)

                    call store_undersampled_velocity_traction_Sh_w(field_w, cosPhi, sinPhi, cos2Phi, sin2Phi, npts2, nvar, dt, &
                    nmx(ip), nmy(ip), nmz(ip), NF_FOR_STORING, ip)

                else
                    field_f = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_s * (0 - z0)

                    if (ht_bottom /= zz(ip)) then

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            call compute_lower_half_space_Sh_propagator(N_mat_layers(:,:, ii * nlayer), N_mat, zz(ip), nlayer, &
                            nu_be(1 + (ii - 1) * nlayer:ii * nlayer))

                            N_mat_bottom(:,:, ii) = N_mat(:,:)
                            
                            call compute_field_f_Sh(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2,&
                            mu(nlayer))

                        enddo

                        ht_bottom = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            N_mat(:,:) = N_mat_bottom(:,:, ii)

                            call compute_field_f_Sh(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2,&
                            mu(nlayer))

                        enddo

                    endif
                    
                    call compute_FFT_inv(field, field_f, nf, nf2, npts, npts2, nvar, nn, npow, zign_neg, dt, mpow)

                    call store_undersampled_velocity_traction_Sh(field, cosPhi, sinPhi, cos2Phi, sin2Phi, npts2, nvar, nmx(ip), &
                    nmy(ip), nmz(ip), NF_FOR_STORING, ip)
                    
                endif

            else

                if (USE_FFTW_INV) then
                    field_f_w = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_s * (0 - z0)
                    call get_ilayer(ilayer, hh, zz(ip), H, nlayer)

                    if (ht_temp /= zz(ip)) then

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            call compute_last_Sh_propagator(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, &
                            nlayer, p, eta_al, eta_be, nu_be(1 + (ii - 1) * nlayer:ii * nlayer), hh, ilayer, control)

                            N_mat_temp(:,:, ii) = N_mat(:,:)

                            call compute_field_f_w_Sh(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, &
                            nf2, mu(ilayer))

                        enddo

                        ht_temp = zz(ip)

                    else

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            N_mat(:,:) = N_mat_temp(:,:, ii)

                            call compute_field_f_w_Sh(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, &
                            nf2,mu(ilayer))

                        enddo


                    endif

                    call compute_FFTW_inv(field_w, field_f_w, nf, nf2, npts, npts2, nvar, nn)

                    call store_undersampled_velocity_traction_Sh_w(field_w, cosPhi, sinPhi, cos2Phi, sin2Phi, npts2, nvar, dt, &
                    nmx(ip), nmy(ip), nmz(ip), NF_FOR_STORING, ip)

                else
                    field_f = 0.
                    tdelay = p * (xx(ip) - x0) * cosPhi + p * (yy(ip) - y0) * sinPhi + eta_s * (0 - z0)
                    call get_ilayer(ilayer, hh, zz(ip), H, nlayer)

                    if (ht_temp /= zz(ip)) then

                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            call compute_last_Sh_propagator(N_mat_layers(:,:, 1 + (ii - 1) * nlayer:ii * nlayer), N_mat, mu, &
                            nlayer, p, eta_al, eta_be, nu_be(1 + (ii - 1) * nlayer:ii * nlayer), hh, ilayer,control)

                            N_mat_temp(:,:, ii) = N_mat(:,:)

                            call compute_field_f_Sh(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2,&
                            mu(ilayer))

                        enddo

                        ht_temp = zz(ip)

                    else
                        do ii = 1, nf2

                            call get_pulsation_apod_coeff(om, stf_coeff, fvec(ii), tdelay, td, isw)

                            ! z is the height of position with respect to the lowest layer interface.
                            !BP
                            N_mat(:,:) = N_mat_temp(:,:, ii)

                            call compute_field_f_Sh(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2,&
                            mu(ilayer))

                        enddo

                    endif

                    call compute_FFT_inv(field, field_f, nf, nf2, npts, npts2, nvar, nn, npow, zign_neg, dt, mpow)

                    call store_undersampled_velocity_traction_Sh(field, cosPhi, sinPhi, cos2Phi, sin2Phi, npts2, nvar, nmx(ip), &
                    nmy(ip), nmz(ip), NF_FOR_STORING, ip)

                endif

            endif

        enddo
        !$OMP END PARALLEL DO
    
    endif


    !BP
    if (USE_FFTW_INV) then
        deallocate(fvec, coeff, field_f_w, field_w, dtmp)
    else
        deallocate(fvec, coeff, field_f, field, dtmp)
    endif
    deallocate(tmp_f1, tmp_f2, tmp_f3, tmp_t1, tmp_t2, tmp_t3)

    if (myrank == 0) then
        write(IMAIN, *)
        write(IMAIN, *) " FK computing passed "
        write(IMAIN, *)
        call flush_IMAIN()
    endif

end subroutine FK

!
!-------------------------------------------------------------------------------------------------
!

!BP
! Compute the pulsation and the apodization window
subroutine get_pulsation_apod_coeff(om, stf_coeff, fvec, tdelay, td, isw)

    use constants
    implicit none
    
    integer,                      parameter   :: CUSTOM_CMPLX=8
    
    !inputs
    real(kind=CUSTOM_REAL),       intent(in)  :: fvec
    real(kind=CUSTOM_REAL),       intent(in)  :: tdelay, td
    integer,                      intent(in)  :: isw
    
    !output
    real(kind=CUSTOM_REAL),       intent(out) :: om
    complex(kind = CUSTOM_CMPLX), intent(out) :: stf_coeff

    real(kind=CUSTOM_REAL)                    :: Tau

    om = 2 * pi * fvec !! pulsation
        
    Tau = (td * 2. * pi) / 3.5
    
    if (isw == 1) then
        stf_coeff = exp(-(om * Tau / (4.0 *pi))**2);
        stf_coeff =  Tau / (2.0 * sqrt(pi) ) * stf_coeff * exp(cmplx(0,-1)*om*tdelay)
    endif

end subroutine get_pulsation_apod_coeff


!BP
subroutine compute_field_f_w_P(field_f_w, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, xi1_ip, xim_ip )
    
    use constants
    use FFTW
    implicit none
    
    integer,                      parameter   :: CUSTOM_CMPLX=8
    
    !inputs
    integer, intent(in) :: nf, nvar, ii, nf2
    real(kind = CUSTOM_REAL), intent(in) :: om, p, xi1_ip, xim_ip
    complex(kind = CUSTOM_CMPLX), intent(in) :: C_3, stf_coeff, N_mat(4, 4), coeff(2, nf2)
    logical, intent(in) :: comp_stress
    
    
    !output
    complex(kind = C_DOUBLE_COMPLEX), intent(out) :: field_f_w(nf, nvar)
       
    
    !local vars
    complex(kind = CUSTOM_CMPLX) :: dx_f, dz_f, txz_f, tzz_f
    
    
    dx_f = N_mat(1, 2) * coeff(1, ii) + N_mat(1, 4) * coeff(2, ii) + N_mat(1, 3) * C_3 ! y_1
    dz_f = N_mat(2, 2) * coeff(1, ii) + N_mat(2, 4) * coeff(2, ii) + N_mat(2, 3) * C_3 ! y_3
    field_f_w(ii, 1) = stf_coeff * dx_f * cmplx(0, -1) * cmplx(0, om) ! (i om)u_x
    field_f_w(ii, 2) = stf_coeff * dz_f * cmplx(0, om) ! (i om)u_z

    if (comp_stress) then
        txz_f = N_mat(3, 2) * coeff(1, ii) + N_mat(3, 4) * coeff(2, ii) + N_mat(3, 3) * C_3 ! tilde{y}_4
        tzz_f = N_mat(4, 2) * coeff(1, ii) + N_mat(4, 4) * coeff(2, ii) + N_mat(4, 3) * C_3 ! tilde{y}_6
        field_f_w(ii, 3) = stf_coeff * om * p * (xi1_ip * tzz_f - 4 * xim_ip * dx_f) ! T_xx
        field_f_w(ii, 4) = stf_coeff * om * p * txz_f * cmplx(0, -1) ! T_xz
        field_f_w(ii, 5) = stf_coeff * om * p * tzz_f ! T_zz
    endif
    
end subroutine compute_field_f_w_P


!BP
subroutine compute_field_f_P(field_f, N_mat, coeff, C_3, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, xi1_ip, xim_ip)
    
    use constants
    use FFTW
    implicit none
    
    integer,                      parameter   :: CUSTOM_CMPLX=8
    
    !inputs
    integer, intent(in) :: nf, nvar, ii, nf2
    real(kind = CUSTOM_REAL), intent(in) :: om, p, xi1_ip, xim_ip
    complex(kind = CUSTOM_CMPLX), intent(in) :: C_3, stf_coeff, N_mat(4, 4), coeff(2, nf2)
    logical, intent(in) :: comp_stress
    
    !output
    complex(kind = CUSTOM_CMPLX), intent(out) :: field_f(nf, nvar)
       
    
    !local vars
    complex(kind = CUSTOM_CMPLX) :: dx_f, dz_f, txz_f, tzz_f
    
    
    dx_f = N_mat(1, 2) * coeff(1, ii) + N_mat(1, 4) * coeff(2, ii) + N_mat(1, 3) * C_3 ! y_1
    dz_f = N_mat(2, 2) * coeff(1, ii) + N_mat(2, 4) * coeff(2, ii) + N_mat(2, 3) * C_3 ! y_3
    field_f(ii, 1) = stf_coeff * dx_f * cmplx(0, -1) * cmplx(0, om) ! (i om)u_x
    field_f(ii, 2) = stf_coeff * dz_f * cmplx(0, om) ! (i om)u_z

    if (comp_stress) then
        txz_f = N_mat(3, 2) * coeff(1, ii) + N_mat(3, 4) * coeff(2, ii) + N_mat(3, 3) * C_3 ! tilde{y}_4
        tzz_f = N_mat(4, 2) * coeff(1, ii) + N_mat(4, 4) * coeff(2, ii) + N_mat(4, 3) * C_3 ! tilde{y}_6
        field_f(ii, 3) = stf_coeff * om * p * (xi1_ip * tzz_f - 4 * xim_ip * dx_f) ! T_xx
        field_f(ii, 4) = stf_coeff * om * p * txz_f * cmplx(0, -1) ! T_xz
        field_f(ii, 5) = stf_coeff * om * p * tzz_f ! T_zz
    endif
    
end subroutine compute_field_f_P


!BP
subroutine compute_field_f_w_Sv(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, xi1_ip, xim_ip )
    
    use constants
    use FFTW
    implicit none
    
    integer,                      parameter   :: CUSTOM_CMPLX=8
    
    !inputs
    integer, intent(in) :: nf, nvar, ii, nf2
    real(kind = CUSTOM_REAL), intent(in) :: om, p, xi1_ip, xim_ip, C_1
    complex(kind = CUSTOM_CMPLX), intent(in) :: stf_coeff, N_mat(4, 4), coeff(2, nf2)
    logical, intent(in) :: comp_stress


    !output
    complex(kind = C_DOUBLE_COMPLEX), intent(out) :: field_f_w(nf, nvar)


    !local vars
    complex(kind = CUSTOM_CMPLX) :: dx_f, dz_f, txz_f, tzz_f


    dx_f = N_mat(1, 2) * coeff(1, ii) + N_mat(1, 4) * coeff(2, ii) + N_mat(1, 1) * C_1 ! y_1
    dz_f = N_mat(2, 2) * coeff(1, ii) + N_mat(2, 4) * coeff(2, ii) + N_mat(2, 1) * C_1 ! y_3
    field_f_w(ii, 1) = stf_coeff * dx_f * cmplx(0, -1) * cmplx(0, om) ! (i om)u_x(1.20)
    field_f_w(ii, 2) = stf_coeff * dz_f * cmplx(0, om) ! (i om)u_z

    if (comp_stress) then
        txz_f = N_mat(3, 2) * coeff(1, ii) + N_mat(3, 4) * coeff(2, ii) + N_mat(3, 1) * C_1 ! tilde{y}_4
        tzz_f = N_mat(4, 2) * coeff(1, ii) + N_mat(4, 4) * coeff(2, ii) + N_mat(4, 1) * C_1 ! tilde{y}_6
        field_f_w(ii, 3) = stf_coeff * om * p * (xi1_ip * tzz_f - 4 * xim_ip * dx_f) !T_xx
        field_f_w(ii, 4) = stf_coeff * om * p * txz_f * cmplx(0, -1) ! T_xz
        field_f_w(ii, 5) = stf_coeff * om * p * tzz_f ! T_zz
    endif

end subroutine compute_field_f_w_Sv


!BP
subroutine compute_field_f_Sv(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, xi1_ip, xim_ip )
    
    use constants
    use FFTW
    implicit none
    
    integer,                      parameter   :: CUSTOM_CMPLX=8
    
    !inputs
    integer, intent(in) :: nf, nvar, ii, nf2
    real(kind = CUSTOM_REAL), intent(in) :: om, p, xi1_ip, xim_ip, C_1
    complex(kind = CUSTOM_CMPLX), intent(in) :: stf_coeff, N_mat(4, 4), coeff(2, nf2)
    logical, intent(in) :: comp_stress


    !output
    complex(kind = CUSTOM_CMPLX), intent(out) :: field_f(nf, nvar)


    !local vars
    complex(kind = CUSTOM_CMPLX) :: dx_f, dz_f, txz_f, tzz_f


    dx_f = N_mat(1, 2) * coeff(1, ii) + N_mat(1, 4) * coeff(2, ii) + N_mat(1, 1) * C_1 ! y_1
    dz_f = N_mat(2, 2) * coeff(1, ii) + N_mat(2, 4) * coeff(2, ii) + N_mat(2, 1) * C_1 ! y_3
    field_f(ii, 1) = stf_coeff * dx_f * cmplx(0, -1) * cmplx(0, om) ! (i om)u_x(1.20)
    field_f(ii, 2) = stf_coeff * dz_f * cmplx(0, om) ! (i om)u_z

    if (comp_stress) then
        txz_f = N_mat(3, 2) * coeff(1, ii) + N_mat(3, 4) * coeff(2, ii) + N_mat(3, 1) * C_1 ! tilde{y}_4
        tzz_f = N_mat(4, 2) * coeff(1, ii) + N_mat(4, 4) * coeff(2, ii) + N_mat(4, 1) * C_1 ! tilde{y}_6
        field_f(ii, 3) = stf_coeff * om * p * (xi1_ip * tzz_f - 4 * xim_ip * dx_f) !T_xx
        field_f(ii, 4) = stf_coeff * om * p * txz_f * cmplx(0, -1) ! T_xz
        field_f(ii, 5) = stf_coeff * om * p * tzz_f ! T_zz
    endif

end subroutine compute_field_f_Sv


!BP
subroutine compute_field_f_w_Sh(field_f_w, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, mu )
    
    use constants
    use FFTW
    implicit none
    
    integer,                      parameter   :: CUSTOM_CMPLX=8
    
    !inputs
    integer, intent(in) :: nf, nvar, ii, nf2
    real(kind = CUSTOM_REAL), intent(in) :: om, p, mu, C_1
    complex(kind = CUSTOM_CMPLX), intent(in) :: stf_coeff, N_mat(2, 2), coeff(2, nf2)
    logical, intent(in) :: comp_stress


    !output
    complex(kind = C_DOUBLE_COMPLEX), intent(out) :: field_f_w(nf, nvar)


    !local vars
    complex(kind = CUSTOM_CMPLX) :: y2, y5


    y2 = N_mat(1,2) * coeff(1, ii) +  N_mat(1, 1) * C_1 
    field_f_w(ii, 1) = stf_coeff * y2 * cmplx(0, 1) * om

    if (comp_stress) then
        y5 =  N_mat(2, 2) * coeff(1, ii) + N_mat(2, 1) * C_1 
        
        field_f_w(ii, 2) = stf_coeff * om * p * y2 * cmplx(0,-1) * mu
        field_f_w(ii, 3) = stf_coeff * om * p * y5
    endif
    
end subroutine compute_field_f_w_Sh


!BP
subroutine compute_field_f_Sh(field_f, N_mat, coeff, C_1, stf_coeff, comp_stress, nf, nvar, ii, om, p, nf2, mu )
    
    use constants
    use FFTW
    implicit none
    
    integer,                      parameter   :: CUSTOM_CMPLX=8
    
    !inputs
    integer, intent(in) :: nf, nvar, ii, nf2
    real(kind = CUSTOM_REAL), intent(in) :: om, p, mu, C_1
    complex(kind = CUSTOM_CMPLX), intent(in) :: stf_coeff, N_mat(2, 2), coeff(2, nf2)
    logical, intent(in) :: comp_stress


    !output
    complex(kind = CUSTOM_CMPLX), intent(out) :: field_f(nf, nvar)


    !local vars
    complex(kind = CUSTOM_CMPLX) :: y2, y5


    y2 = N_mat(1,2) * coeff(1, ii) +  N_mat(1, 1) * C_1 
    field_f(ii, 1) = stf_coeff * y2 * cmplx(0, 1) * om 
    

    if (comp_stress) then
        y5 = N_mat(2, 2) * coeff(1, ii) + N_mat(2, 1) * C_1 
        
        field_f(ii, 2) = stf_coeff * om * p * y2 * cmplx(0,-1) * mu
        field_f(ii, 3) = stf_coeff * om * p * y5
    endif
    
    
end subroutine compute_field_f_Sh




!BP
subroutine compute_FFTW_inv(field_w, field_f_w, nf, nf2, npts, npts2, nvar, nn)

    use constants
    use FFTW
    implicit none
    
    !inputs
    integer, intent(in) :: nf, nf2, npts, npts2, nvar, nn
    complex(kind = C_DOUBLE_COMPLEX), intent(inout) :: field_f_w(nf, nvar)
    
    
    !outputs
    real(kind = C_DOUBLE), intent(out) :: field_w(npts2, nvar)
    
    !local vars
    integer :: j, ii
    real(kind=CUSTOM_REAL) :: dtmp(npts)


    ! pad negative f, and convert to time series
    do ii = 2, nf2 - 1
        field_f_w(nf + 2 - ii,:) = conjg(field_f_w(ii,:))
    enddo

    field_w = 0.

    do j = 1, nvar
        call FFTW_inv(field_f_w(:, j), field_w(:, j), npts2, nf)
        ! wrap around to start from t0: here one has to be careful if t0/dt is not
        ! exactly an integer, assume nn > 0
        if (nn > 0) then
            dtmp(1:nn) = field_w(npts2 - nn + 1:npts2, j)
            field_w(nn + 1:npts2, j) = field_w(1:npts2 - nn, j)
            field_w(1:nn, j) = dtmp(1:nn)
        else if (nn < 0) then
            dtmp(1:nn) = field_w(1:nn, j)
            field_w(1:npts - nn, j) = field_w(nn + 1:npts, j)
            field_w(npts - nn + 1:npts, j) = dtmp(1:nn)
        endif
    enddo


end subroutine compute_FFTW_inv


!BP
subroutine compute_FFT_inv(field, field_f, nf, nf2, npts, npts2, nvar, nn, npow, zign_neg, dt, mpow)

    use constants
    implicit none
    
    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8
    
    !inputs
    integer, intent(in)                         :: nf, nf2, npts, npts2, nvar, nn, npow
    complex(kind = CUSTOM_CMPLX), intent(inout) :: field_f(nf, nvar)
    real(kind=CUSTOM_REAL), intent(in)          :: zign_neg, dt, mpow(30)
    
    
    !outputs
    real(kind = CUSTOM_REAL), intent(out)       :: field(npts2, nvar)
    
    !local vars
    integer                                     :: j, ii
    real(kind = CUSTOM_REAL)                    :: dtmp(npts)

    ! pad negative f, and convert to time series
    do ii = 2, nf2 - 1
        field_f(nf + 2 - ii,:) = conjg(field_f(ii,:))
    enddo

    !! inverse fast fourier transform
    field = 0.
    do j = 1, nvar
        call FFTinv(npow, field_f(:, j), zign_neg, dt, field(:, j), mpow)
        ! wrap around to start from t0: here one has to be careful if t0/dt is not
        ! exactly an integer, assume nn > 0
        if (nn > 0) then
            dtmp(1:nn) = field(npts2 - nn + 1:npts2, j)
            field(nn + 1:npts2, j) = field(1:npts2 - nn, j)
            field(1:nn, j) = dtmp(1:nn)
        else if (nn < 0) then
            dtmp(1:nn) = field(1:nn, j)
            field(1:npts - nn, j) = field(nn + 1:npts, j)
            field(npts - nn + 1:npts, j) = dtmp(1:nn)
        endif
    enddo
end subroutine compute_FFT_inv


!BP
!!Only permits to store and compute velocity and traction for P and Sv-waves
subroutine store_undersampled_velocity_traction_FK_w(field_w, cosPhi, sinPhi, npts2, nvar, dt, nmx_ip, nmy_ip, nmz_ip, &
    bdlambdamu_ip, NF_FOR_STORING, ip)

    use specfem_par_coupling, only: VX_t, VY_t, VZ_t, TX_t, TY_t, TZ_t
    use constants
    use FFTW
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    !inputs
    integer, intent(in) :: nvar, npts2, NF_FOR_STORING, ip
    real(kind = CUSTOM_REAL), intent(in) :: cosPhi, sinPhi, dt
    real(kind=CUSTOM_REAL),intent(in) :: nmx_ip, nmy_ip, nmz_ip, bdlambdamu_ip
    
    real(kind = C_DOUBLE), intent(inout) :: field_w(npts2, nvar)
    
    !local vars
    integer :: ier, lpts
    real(kind = CUSTOM_REAL), dimension(:), allocatable :: tmp_t1, tmp_t2
    real(kind = CUSTOM_REAL) :: sigma_rr, sigma_rt, sigma_rz, sigma_tt, sigma_tz, sigma_zz
    real(kind = CUSTOM_REAL) :: Txx_tmp, Txy_tmp, Txz_tmp, Tyy_tmp, Tyz_tmp, Tzz_tmp


    allocate(tmp_t1(npts2), tmp_t2(npts2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2223')
    if (ier /= 0) stop 'error while allocating'

    !!BP 
    !! Normalize field as results of FFTW are not normalized
    !! store undersampled version of velocity  FK solution
    tmp_t1(:) = field_w(:, 1) * cosPhi / (npts2 * dt)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vx_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(:) = field_w(:, 1) * sinPhi/ (npts2 * dt)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vy_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(:) = field_w(:, 2) / (npts2 * dt)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vz_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    !! compute traction
    do lpts = 1, NF_FOR_STORING

        sigma_rr = field_w(lpts, 3)/ (npts2 * dt)
        sigma_rt = 0.0
        sigma_rz = field_w(lpts, 4)/ (npts2 * dt)
        sigma_zz = field_w(lpts, 5)/ (npts2 * dt)
        sigma_tt = bdlambdamu_ip*(sigma_rr + sigma_zz)
        sigma_tz = 0.0

        Txx_tmp = sigma_rr * cosPhi * cosPhi + sigma_tt * sinPhi * sinPhi
        Txy_tmp = cosPhi * sinPhi * (sigma_rr - sigma_tt)
        Txz_tmp = sigma_rz * cosPhi
        Tyy_tmp = sigma_rr * sinPhi * sinPhi + sigma_tt * cosPhi * cosPhi
        Tyz_tmp = sigma_rz * sinPhi
        Tzz_tmp = sigma_zz

        !! store directly the traction
        Tx_t(ip, lpts) = Txx_tmp * nmx_ip + Txy_tmp * nmy_ip + Txz_tmp * nmz_ip
        Ty_t(ip, lpts) = Txy_tmp * nmx_ip + Tyy_tmp * nmy_ip + Tyz_tmp * nmz_ip
        Tz_t(ip, lpts) = Txz_tmp * nmx_ip + Tyz_tmp * nmy_ip + Tzz_tmp * nmz_ip

    enddo

    !! store undersamped version of tractions FK solution
    tmp_t1(1:NF_FOR_STORING) = Tx_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Tx_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(1:NF_FOR_STORING) = Ty_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Ty_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(1:NF_FOR_STORING) = Tz_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Tz_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)
    
    deallocate(tmp_t1, tmp_t2)
       
end subroutine store_undersampled_velocity_traction_FK_w
!
!
!
!!BP 
!!Only permits to store and compute velocity and traction for P and Sv-waves
subroutine store_undersampled_velocity_traction_FK(field, cosPhi, sinPhi, npts2, nvar, nmx_ip, nmy_ip, nmz_ip, &
    bdlambdamu_ip, NF_FOR_STORING, ip)

    use specfem_par_coupling, only: VX_t, VY_t, VZ_t, TX_t, TY_t, TZ_t
    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    !inputs
    integer, intent(in) :: nvar, npts2, NF_FOR_STORING, ip
    real(kind = CUSTOM_REAL), intent(in) :: cosPhi, sinPhi
    real(kind=CUSTOM_REAL),intent(in) :: nmx_ip, nmy_ip, nmz_ip, bdlambdamu_ip
    
    real(kind = CUSTOM_REAL), intent(inout) :: field(npts2, nvar)

    !local vars
    integer :: ier, lpts
    real(kind = CUSTOM_REAL), dimension(:), allocatable :: tmp_t1, tmp_t2
    real(kind = CUSTOM_REAL) :: sigma_rr, sigma_rt, sigma_rz, sigma_tt, sigma_tz, sigma_zz
    real(kind = CUSTOM_REAL) :: Txx_tmp, Txy_tmp, Txz_tmp, Tyy_tmp, Tyz_tmp, Tzz_tmp


    allocate(tmp_t1(npts2), tmp_t2(npts2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2223')
    if (ier /= 0) stop 'error while allocating'

    !!BP 
    !! Normalize field
    !! store undersampled version of velocity  FK solution
    tmp_t1(:) = field(:, 1) * cosPhi
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vx_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(:) = field(:, 1) * sinPhi
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vy_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(:) = field(:, 2)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vz_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    !! compute traction
    do lpts = 1, NF_FOR_STORING

        sigma_rr = field(lpts, 3)
        sigma_rt = 0.0
        sigma_rz = field(lpts, 4)
        sigma_zz = field(lpts, 5)
        sigma_tt = bdlambdamu_ip*(sigma_rr + sigma_zz)
        sigma_tz = 0.0

        Txx_tmp = sigma_rr * cosPhi * cosPhi + sigma_tt * sinPhi * sinPhi
        Txy_tmp = cosPhi * sinPhi*(sigma_rr - sigma_tt)
        Txz_tmp = sigma_rz * cosPhi
        Tyy_tmp = sigma_rr * sinPhi * sinPhi + sigma_tt * cosPhi * cosPhi
        Tyz_tmp = sigma_rz * sinPhi
        Tzz_tmp = sigma_zz

        !! store directly the traction
        Tx_t(ip, lpts) = Txx_tmp * nmx_ip + Txy_tmp * nmy_ip + Txz_tmp * nmz_ip
        Ty_t(ip, lpts) = Txy_tmp * nmx_ip + Tyy_tmp * nmy_ip + Tyz_tmp * nmz_ip
        Tz_t(ip, lpts) = Txz_tmp * nmx_ip + Tyz_tmp * nmy_ip + Tzz_tmp * nmz_ip

    enddo

    !! store undersamped version of tractions FK solution
    tmp_t1(1:NF_FOR_STORING) = Tx_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Tx_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(1:NF_FOR_STORING) = Ty_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Ty_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(1:NF_FOR_STORING) = Tz_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Tz_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    deallocate(tmp_t1, tmp_t2)
    
end subroutine store_undersampled_velocity_traction_FK


subroutine store_undersampled_velocity_traction_Sh_w(field_w, cosPhi, sinPhi, cos2Phi, sin2Phi, npts2, nvar, dt, nmx_ip, nmy_ip, &
    nmz_ip, NF_FOR_STORING, ip)

    use specfem_par_coupling, only: VX_t, VY_t, VZ_t, TX_t, TY_t, TZ_t
    use constants
    use FFTW
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    !inputs
    integer, intent(in) :: nvar, npts2, NF_FOR_STORING, ip
    real(kind = CUSTOM_REAL), intent(in) :: cosPhi, sinPhi, cos2Phi, sin2Phi, dt
    real(kind=CUSTOM_REAL),intent(in) :: nmx_ip, nmy_ip, nmz_ip
    
    real(kind = C_DOUBLE), intent(inout) :: field_w(npts2, nvar)
    
    !local vars
    integer :: ier, lpts
    real(kind = CUSTOM_REAL), dimension(:), allocatable :: tmp_t1, tmp_t2
    real(kind = CUSTOM_REAL) :: sigma_rr, sigma_zz
    real(kind = CUSTOM_REAL) :: Txx_tmp, Txy_tmp, Txz_tmp, Tyy_tmp, Tyz_tmp


    allocate(tmp_t1(npts2), tmp_t2(npts2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2223')
    if (ier /= 0) stop 'error while allocating'

    !!BP 
    !! Normalize field as results of FFTW are not normalized
    !! store undersampled version of velocity  FK solution
    tmp_t1(:) = -field_w(:, 1) * sinPhi / (npts2 * dt)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vx_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(:) = field_w(:, 1) * cosPhi/ (npts2 * dt)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vy_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(:) = 0.0
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vz_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    !! compute traction
    do lpts = 1, NF_FOR_STORING

        sigma_rr = field_w(lpts, 2)/ (npts2 * dt)
        sigma_zz = field_w(lpts, 3)/ (npts2 * dt)

        Txx_tmp = -sigma_rr * sin2Phi
        Txy_tmp = sigma_rr * cos2Phi
        Txz_tmp = -sigma_zz * sinPhi
        Tyy_tmp = sigma_rr * sin2Phi
        Tyz_tmp = sigma_zz * cosPhi

        !! store directly the traction
        Tx_t(ip, lpts) = Txx_tmp * nmx_ip + Txy_tmp * nmy_ip + Txz_tmp * nmz_ip
        Ty_t(ip, lpts) = Txy_tmp * nmx_ip + Tyy_tmp * nmy_ip + Tyz_tmp * nmz_ip
        Tz_t(ip, lpts) = Txz_tmp * nmx_ip + Tyz_tmp * nmy_ip

    enddo

    !! store undersamped version of tractions FK solution
    tmp_t1(1:NF_FOR_STORING) = Tx_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Tx_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(1:NF_FOR_STORING) = Ty_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Ty_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(1:NF_FOR_STORING) = Tz_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Tz_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)
    
    deallocate(tmp_t1, tmp_t2)
    
    
end subroutine store_undersampled_velocity_traction_Sh_w


subroutine store_undersampled_velocity_traction_Sh(field, cosPhi, sinPhi, cos2Phi, sin2Phi, npts2, nvar, nmx_ip, nmy_ip, nmz_ip, &
    NF_FOR_STORING, ip)

    use specfem_par_coupling, only: VX_t, VY_t, VZ_t, TX_t, TY_t, TZ_t
    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    !inputs
    integer, intent(in) :: nvar, npts2, NF_FOR_STORING, ip
    real(kind = CUSTOM_REAL), intent(in) :: cosPhi, sinPhi, cos2Phi, sin2Phi
    real(kind=CUSTOM_REAL),intent(in) :: nmx_ip, nmy_ip, nmz_ip
    
    real(kind = CUSTOM_REAL), intent(inout) :: field(npts2, nvar)

    !local vars
    integer :: ier, lpts
    real(kind = CUSTOM_REAL), dimension(:), allocatable :: tmp_t1, tmp_t2
    real(kind = CUSTOM_REAL) :: sigma_rr, sigma_zz
    real(kind = CUSTOM_REAL) :: Txx_tmp, Txy_tmp, Txz_tmp, Tyy_tmp, Tyz_tmp


    allocate(tmp_t1(npts2), tmp_t2(npts2), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2223')
    if (ier /= 0) stop 'error while allocating'

    !!BP 
    !! Normalize field
    !! store undersampled version of velocity  FK solution
    tmp_t1(:) = -field(:, 1) * sinPhi
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vx_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(:) = field(:, 1) * cosPhi
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vy_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(:) = 0.0
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    vz_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    !! compute traction
    do lpts = 1, NF_FOR_STORING

        sigma_rr = field(lpts, 2)
        sigma_zz = field(lpts, 3)

        Txx_tmp = -sigma_rr * sin2Phi
        Txy_tmp = sigma_rr * cos2Phi
        Txz_tmp = -sigma_zz * sinPhi
        Tyy_tmp = sigma_rr * sin2Phi
        Tyz_tmp = sigma_zz * cosPhi

        !! store directly the traction
        Tx_t(ip, lpts) = Txx_tmp * nmx_ip + Txy_tmp * nmy_ip + Txz_tmp * nmz_ip
        Ty_t(ip, lpts) = Txy_tmp * nmx_ip + Tyy_tmp * nmy_ip + Tyz_tmp * nmz_ip
        Tz_t(ip, lpts) = Txz_tmp * nmx_ip + Tyz_tmp * nmy_ip

    enddo

    !! store undersamped version of tractions FK solution
    tmp_t1(1:NF_FOR_STORING) = Tx_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Tx_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(1:NF_FOR_STORING) = Ty_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Ty_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    tmp_t1(1:NF_FOR_STORING) = Tz_t(ip, 1:NF_FOR_STORING)
    call compute_spline_coef_to_store(tmp_t1, npts2, tmp_t2)
    Tz_t(ip, 1:NF_FOR_STORING) = tmp_t2(1:NF_FOR_STORING)

    deallocate(tmp_t1, tmp_t2)
    
end subroutine store_undersampled_velocity_traction_Sh



!BP
subroutine compute_N_rayleigh(alpha, beta, mu, H, nlayer, nf2, fvec, p, eta_al, eta_be, nu_al, nu_be, gamma0, gamma1, Nmat_layers)

    ! assumes that ht = 0 is the bottom interface

    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    ! input
    integer, intent(in) :: nlayer, nf2
    real(kind = CUSTOM_REAL), dimension(nlayer), intent(in) :: alpha, beta, mu, H
    real(kind = CUSTOM_REAL), dimension(nf2) :: fvec
    real(kind = CUSTOM_REAL), intent(in) :: p

    ! output
    complex(kind = CUSTOM_CMPLX), dimension(4, 4, nlayer * nf2), intent(inout) :: Nmat_layers(4, 4, nlayer * nf2)
    complex(kind = CUSTOM_CMPLX), dimension(nlayer), intent(inout) :: eta_al, eta_be
    complex(kind = CUSTOM_CMPLX), dimension(nlayer * nf2), intent(inout) :: nu_al, nu_be
    real(kind = CUSTOM_REAL), dimension(nlayer), intent(inout) :: gamma0, gamma1

    ! local vars
    integer :: i, j, ii
    complex(kind = CUSTOM_CMPLX), dimension(4, 4) :: Emat, Player
    complex(kind = CUSTOM_CMPLX) :: ca, sa, xa, ya, cb, sb, xb, yb, g1, mul, c1, c2
    real(kind = CUSTOM_REAL), dimension(nlayer) :: hh
    real(kind = CUSTOM_REAL) :: om

    if (nlayer < 1) stop 'nlayer has to be larger than or equal to 1'

    !loop over frequencies
    do ii = 1, nf2

        om = 2 * pi * fvec(ii)

        do i = 1, nlayer
            eta_al(i) = -cmplx(0, 1) * sqrt(1.0/alpha(i) + p) * sqrt(1.0/alpha(i) - p) ! i*vertical slowness, purely imaginary
            eta_be(i) = -cmplx(0, 1) * sqrt(1.0/beta(i) + p) * sqrt(1.0/beta(i) - p)
            nu_al(i + (ii - 1) * nlayer) = om * eta_al(i)
            nu_be(i + (ii - 1) * nlayer) = om * eta_be(i) ! i * vertical wavenumber
            gamma0(i) = 2 * beta(i)**2 * p**2
            gamma1(i) = 1 - 1/gamma0(i)
        enddo

        ! note Emat is not omega dependent
        ! BP! changed order to respect column major
        Emat(1, 1) = eta_be(nlayer)/p
        Emat(2, 1) = 1
        Emat(3, 1) = 2 * mu(nlayer) * gamma1(nlayer)
        Emat(4, 1) = 2 * mu(nlayer) * eta_be(nlayer)/p
        Emat(1, 2) = -Emat(1, 1)
        Emat(2, 2) = 1
        Emat(3, 2) = Emat(3, 1)
        Emat(4, 2) = -Emat(4, 1)

        Emat(1, 3) = 1
        Emat(2, 3) = eta_al(nlayer)/p
        Emat(3, 3) = 2 * mu(nlayer) * eta_al(nlayer)/p
        Emat(4, 3) = Emat(3, 1)
        Emat(1, 4) = 1
        Emat(2, 4) = -Emat(2, 3)
        Emat(3, 4) = -Emat(3, 3)
        Emat(4, 4) = Emat(3, 1)

        Nmat_layers(:,:, nlayer + (ii - 1) * nlayer) = Emat

        hh = H

        do j = nlayer - 1, 1, -1
            c1 = nu_al(j + (ii - 1) * nlayer) * hh(j)
            ca = (exp(c1) + exp(-c1))/2
            sa = (exp(c1) - exp(-c1))/2
            xa = eta_al(j) * sa/p
            ya = p * sa/eta_al(j)
            c2 = nu_be(j + (ii - 1) * nlayer) * hh(j)
            cb = (exp(c2) + exp(-c2))/2
            sb = (exp(c2) - exp(-c2))/2
            xb = eta_be(j) * sb/p
            yb = p * sb/eta_be(j)
            g1 = gamma1(j)
            mul = mu(j)

            ! BP! changed order to respect column major
            Player(1, 1) = ca - g1 * cb
            Player(2, 1) = xa - g1 * yb
            Player(3, 1) = 2 * mul * (xa - g1**2 * yb)
            Player(4, 1) = 2 * mul * g1 * (ca - cb)
            Player(1, 2) = xb - g1 * ya
            Player(2, 2) = cb - g1 * ca
            Player(3, 2) = 2 * mul * g1 * (cb - ca)
            Player(4, 2) = 2 * mul * (xb - g1**2 * ya)
            Player(1, 3) = (ya - xb)/(2 * mul)
            Player(2, 3) = (ca - cb)/(2 * mul)
            Player(3, 3) = ca - g1 * cb
            Player(4, 3) = g1 * ya - xb
            Player(1, 4) = (cb - ca)/(2 * mul)
            Player(2, 4) = (yb - xa)/(2 * mul)
            Player(3, 4) = g1 * yb - xa
            Player(4, 4) = cb - g1 * ca

            ! if (pout) print *,'j,player',j,player

            Nmat_layers(:,:, j + (ii - 1) * nlayer) = gamma0(j) * matmul(Player, Nmat_layers(:,:, j + 1 + (ii - 1) * nlayer))
        enddo

    enddo

    !endif

end subroutine compute_N_rayleigh

!BP
subroutine compute_last_N_rayleigh(Nmat_layers, Nmat, mu, nlayer, p, eta_al, eta_be, nu_al, nu_be, gamma0, gamma1, hh, &
    ilayer)

    ! assumes that ht = 0 is the bottom interface

    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    ! input
    integer, intent(in) :: nlayer
    real(kind = CUSTOM_REAL), dimension(nlayer), intent(in) :: mu
    real(kind = CUSTOM_REAL), intent(in) :: p
    complex(kind = CUSTOM_CMPLX), dimension(4, 4, nlayer), intent(in) :: Nmat_layers(4, 4, nlayer)
    complex(kind = CUSTOM_CMPLX), dimension(nlayer), intent(in) :: eta_al, eta_be, nu_al, nu_be
    real(kind = CUSTOM_REAL), dimension(nlayer), intent(in) :: gamma0, gamma1
    real(kind = CUSTOM_REAL), intent(in) :: hh
    integer, intent(in) :: ilayer

    !output
    complex(kind = CUSTOM_CMPLX), dimension(4, 4), intent(inout) :: Nmat(4, 4)

    ! local vars
    complex(kind = CUSTOM_CMPLX), dimension(4, 4) :: Player

    complex(kind = CUSTOM_CMPLX) :: ca, sa, xa, ya, cb, sb, xb, yb, g1, mul, c1, c2
    complex(kind = CUSTOM_CMPLX) :: expc1, expmc1, expc2, expmc2


    ! in layers

    !    !hh = H
    !    ilayer = nlayer
    !    do j = nlayer - 1, 1, -1
    !        if (ht <= sum(H(j:nlayer - 1))) then
    !            ilayer = j; exit
    !        endif
    !    enddo
    !    !hh(ilayer + 1:nlayer - 1) = H(ilayer + 1:nlayer - 1)
    !    hh = ht - sum(H(ilayer + 1:nlayer - 1))
    !    if (hh < 0) stop 'Error setting layer thickness'

    c1 = nu_al(ilayer) * hh
    expc1 = exp(c1)
    expmc1 = exp(-c1)
    ca = (expc1 + expmc1)/2
    sa = (expc1 - expmc1)/2
    xa = eta_al(ilayer) * sa/p
    ya = p * sa/eta_al(ilayer)
    c2 = nu_be(ilayer) * hh
    expc2 = exp(c2)
    expmc2 = exp(-c2)
    cb = (expc2 + expmc2)/2
    sb = (expc2 - expmc2)/2
    xb = eta_be(ilayer) * sb/p
    yb = p * sb/eta_be(ilayer)
    g1 = gamma1(ilayer)
    mul = mu(ilayer)

    ! BP! changed order to respect column major
    Player(1, 1) = ca - g1 * cb
    Player(2, 1) = xa - g1 * yb
    Player(3, 1) = 2 * mul * (xa - g1**2 * yb)
    Player(4, 1) = 2 * mul * g1 * (ca - cb)
    Player(1, 2) = xb - g1 * ya
    Player(2, 2) = cb - g1 * ca
    Player(3, 2) = 2 * mul * g1 * (cb - ca)
    Player(4, 2) = 2 * mul * (xb - g1**2 * ya)
    Player(1, 3) = (ya - xb)/(2 * mul)
    Player(2, 3) = (ca - cb)/(2 * mul)
    Player(3, 3) = ca - g1 * cb
    Player(4, 3) = g1 * ya - xb
    Player(1, 4) = (cb - ca)/(2 * mul)
    Player(2, 4) = (yb - xa)/(2 * mul)
    Player(3, 4) = g1 * yb - xa
    Player(4, 4) = cb - g1 * ca

    Nmat = gamma0(ilayer) * matmul(Player, Nmat_layers(:,:, ilayer + 1))


end subroutine compute_last_N_rayleigh


!BP
subroutine compute_Sh_propagator(alpha, beta, mu, H, nlayer, nf2, fvec, p, eta_al, eta_be, nu_be, Nmat_layers)

    ! assumes that ht = 0 is the bottom interface

    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    ! input
    integer, intent(in) :: nlayer, nf2
    real(kind = CUSTOM_REAL), dimension(nlayer), intent(in) :: alpha, beta, mu, H
    real(kind = CUSTOM_REAL), dimension(nf2) :: fvec
    real(kind = CUSTOM_REAL), intent(in) :: p

    ! output
    complex(kind = CUSTOM_CMPLX), dimension(2, 2, nlayer * nf2), intent(inout) :: Nmat_layers(2, 2, nlayer * nf2)
    complex(kind = CUSTOM_CMPLX), dimension(nlayer), intent(inout) :: eta_al, eta_be
    complex(kind = CUSTOM_CMPLX), dimension(nlayer * nf2), intent(inout) :: nu_be

    ! local vars
    integer :: i, j, ii
    complex(kind = CUSTOM_CMPLX), dimension(2, 2) :: Emat, Player
    complex(kind = CUSTOM_CMPLX) :: cb, sb, xb, yb, mul, c2
    real(kind = CUSTOM_REAL), dimension(nlayer) :: hh
    real(kind = CUSTOM_REAL) :: om

    if (nlayer < 1) stop 'nlayer has to be larger than or equal to 1'

    !loop over frequencies
    do ii = 1, nf2

        om = 2 * pi * fvec(ii)

        do i = 1, nlayer
            eta_al(i) = -cmplx(0, 1) * sqrt(1.0/alpha(i) + p) * sqrt(1.0/alpha(i) - p) ! i*vertical slowness, purely imaginary
            eta_be(i) = -cmplx(0, 1) * sqrt(1.0/beta(i) + p) * sqrt(1.0/beta(i) - p)
            nu_be(i + (ii - 1) * nlayer) = om * eta_be(i) ! i * vertical wavenumber
        enddo

        ! note Emat is not omega dependent
        ! BP! changed order to respect column major
        Emat(1, 1) = 1
        Emat(2, 1) = mu(nlayer) * eta_be(nlayer) / p
        Emat(1, 2) = -Emat(1, 1)
        Emat(2, 2) = Emat(2,1)

        Nmat_layers(:,:, nlayer + (ii - 1) * nlayer) = Emat

        hh = H

        do j = nlayer - 1, 1, -1
            
            c2 = nu_be(j + (ii - 1) * nlayer) * hh(j)
            cb = (exp(c2) + exp(-c2))/2
            sb = (exp(c2) - exp(-c2))/2
            xb = eta_be(j) * sb/p
            yb = p * sb/eta_be(j)
            mul = mu(j)

            ! BP! changed order to respect column major
            Player(1, 1) = cb
            Player(2, 1) = mul * xb
            Player(1, 2) = yb / mul
            Player(2, 2) = cb

            ! if (pout) print *,'j,player',j,player
            Nmat_layers(:,:, j + (ii - 1) * nlayer) = matmul(Player, Nmat_layers(:,:, j + 1 + (ii - 1) * nlayer))
        enddo

    enddo
    
end subroutine compute_Sh_propagator


subroutine compute_last_Sh_propagator(Nmat_layers, Nmat, mu, nlayer, p, eta_al, eta_be, nu_be, hh, ilayer, control)

    ! assumes that ht = 0 is the bottom interface

    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    ! input
    integer, intent(in) :: nlayer
    real(kind = CUSTOM_REAL), dimension(nlayer), intent(in) :: mu
    real(kind = CUSTOM_REAL), intent(in) :: p
    complex(kind = CUSTOM_CMPLX), dimension(2, 2, nlayer), intent(in) :: Nmat_layers(2, 2, nlayer)
    complex(kind = CUSTOM_CMPLX), dimension(nlayer), intent(in) :: eta_al, eta_be, nu_be
    real(kind = CUSTOM_REAL), intent(in) :: hh
    integer, intent(in) :: ilayer, control

    !output
    complex(kind = CUSTOM_CMPLX), dimension(2, 2), intent(inout) :: Nmat(2, 2)

    ! local vars
    complex(kind = CUSTOM_CMPLX), dimension(2, 2) :: Player

    complex(kind = CUSTOM_CMPLX) :: cb, sb, xb, yb, mul, c2
    complex(kind = CUSTOM_CMPLX) :: expc2, expmc2


    ! in layers

    !    !hh = H
    !    ilayer = nlayer
    !    do j = nlayer - 1, 1, -1
    !        if (ht <= sum(H(j:nlayer - 1))) then
    !            ilayer = j; exit
    !        endif
    !    enddo
    !    !hh(ilayer + 1:nlayer - 1) = H(ilayer + 1:nlayer - 1)
    !    hh = ht - sum(H(ilayer + 1:nlayer - 1))
    !    if (hh < 0) stop 'Error setting layer thickness'
    c2 = nu_be(ilayer) * hh
    expc2 = exp(c2)
    expmc2 = exp(-c2)
    cb = (expc2 + expmc2)/2
    sb = (expc2 - expmc2)/2
    xb = eta_be(ilayer) * sb/p
    yb = p * sb/eta_be(ilayer)
    mul = mu(ilayer)

    ! BP! changed order to respect column major
    Player(1, 1) = cb
    Player(2, 1) = mul * xb
    Player(1, 2) = yb / mul
    Player(2, 2) = cb

    Nmat = matmul(Player, Nmat_layers(:,:, ilayer + 1))
    
end subroutine compute_last_Sh_propagator




subroutine compute_lower_half_space_N_rayleigh(Nmat_layers, Nmat, ht, nlayer, nu_al, nu_be)

    ! assumes that ht = 0 is the bottom interface

    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    ! input
    integer, intent(in) :: nlayer
    real(kind = CUSTOM_REAL), intent(in) :: ht
    complex(kind = CUSTOM_CMPLX), dimension(4, 4), intent(in) :: Nmat_layers(4, 4)
    complex(kind = CUSTOM_CMPLX), dimension(nlayer), intent(in) :: nu_al, nu_be

    !output
    complex(kind = CUSTOM_CMPLX), dimension(4, 4), intent(inout) :: Nmat(4, 4)

    ! local vars
    complex(kind = CUSTOM_CMPLX), dimension(4, 4) :: Gmat

    Gmat = 0.
    Gmat(1, 1) = exp(nu_be(nlayer) * ht)
    Gmat(2, 2) = exp(-nu_be(nlayer) * ht)
    Gmat(3, 3) = exp(nu_al(nlayer) * ht)
    Gmat(4, 4) = exp(-nu_al(nlayer) * ht)
    Nmat = matmul(Nmat_layers(:,:), Gmat)


end subroutine compute_lower_half_space_N_rayleigh


subroutine compute_lower_half_space_Sh_propagator(Nmat_layers, Nmat, ht, nlayer, nu_be)
    
    ! assumes that ht = 0 is the bottom interface

    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    ! input
    integer, intent(in) :: nlayer
    real(kind = CUSTOM_REAL), intent(in) :: ht
    complex(kind = CUSTOM_CMPLX), dimension(2, 2), intent(in) :: Nmat_layers(2, 2)
    complex(kind = CUSTOM_CMPLX), dimension(nlayer), intent(in) :: nu_be

    !output
    complex(kind = CUSTOM_CMPLX), dimension(2, 2), intent(inout) :: Nmat(2, 2)

    ! local vars
    complex(kind = CUSTOM_CMPLX), dimension(2, 2) :: Gmat

    Gmat = 0.
    Gmat(1, 1) = exp(nu_be(nlayer) * ht)
    Gmat(2, 2) = exp(-nu_be(nlayer) * ht)
    
    Nmat = matmul(Nmat_layers(:,:), Gmat)

    
end subroutine compute_lower_half_space_Sh_propagator


subroutine get_ilayer(ilayer, hh, ht, H, nlayer)

    use constants
    implicit none

    ! precision for complex
    integer, parameter :: CUSTOM_CMPLX = 8

    ! input
    integer, intent(in) :: nlayer
    real(kind = CUSTOM_REAL), intent(in) :: ht
    real(kind = CUSTOM_REAL), dimension(nlayer), intent(in) :: H

    !output
    integer, intent(out) :: ilayer
    real(kind = CUSTOM_REAL), intent(out) :: hh

    !local
    integer :: j

    ilayer = nlayer
    do j = nlayer - 1, 1, -1
        if (ht <= sum(H(j:nlayer - 1))) then
            ilayer = j; exit
        endif
    enddo
    !hh(ilayer + 1:nlayer - 1) = H(ilayer + 1:nlayer - 1)
    hh = ht - sum(H(ilayer + 1:nlayer - 1))
    if (hh < 0) stop 'Error setting layer thickness'



end subroutine get_ilayer
!
!-------------------------------------------------------------------------------------------------
!

subroutine FFT(npow, xi, zign, dtt, mpow)

    ! Fourier transform
    ! This inputs AND outputs a complex function.
    ! The convention is FFT --> e^(-iwt)
    ! numerical factor for Plancherel theorem: planch_fac = dble(NPT * dt * dt)

    use constants, only: CUSTOM_REAL

    implicit none

    integer, parameter :: CUSTOM_CMPLX = 8

    integer :: npow
    integer :: lblock, k, FK, jh, ii, istart
    integer :: l, iblock, nblock, i, lbhalf, j, lx

    complex(kind = CUSTOM_CMPLX), dimension(*) :: xi
    complex(kind = CUSTOM_CMPLX) :: wk, hold, q

    real(kind = CUSTOM_REAL) :: zign, flx, inv_of_flx, v, dtt, pi

    !! DK DK here is the hardwired maximum size of the array
    !! DK DK Aug 2016: if this routine is called many times (for different mesh points at which the SEM is coupled with FK)
    !! DK DK Aug 2016: this should be moved to the calling program and precomputed once and for all
    real(kind = CUSTOM_REAL) :: mpow(30)

    pi = acos(-1.0)

    !! DK DK added this sanity check
    if (npow > 30) stop 'error: the FK FTT routine has an hardwired maximum of 30 levels'
    !! DK DK in any case the line below imposes a maximum of 31, otherwise the integer 2**n will overflow

    lx = 2**npow

    do l = 1, npow

        nblock = 2**(l - 1)
        lblock = lx/nblock
        lbhalf = lblock/2

        k = 0

        do iblock = 1, nblock

            FK = k
            flx = lx

            v = zign * 2. * PI * FK/flx ! Fourier convention
            ! - sign: MATLAB convention: forward e^{-i om t}
            ! + sign: engineering convention: forward e^{i om t}
            wk = cmplx(cos(v), -sin(v)) ! sign change to -sin(v) or sin(v)
            istart = lblock * (iblock - 1)

            do i = 1, lbhalf
                j = istart + i
                jh = j + lbhalf
                q = xi(jh) * wk
                xi(jh) = xi(j) - q
                xi(j) = xi(j) + q
            enddo

            do i = 2, npow
                ii = i
                if (k < mpow(i)) goto 4
                k = k - mpow(i)
            enddo

            4 k = k + mpow(ii)

        enddo
    enddo

    k = 0

    do j = 1, lx
        if (k < j) goto 5

        hold = xi(j)
        xi(j) = xi(k + 1)
        xi(k + 1) = hold

        5 do i = 1, npow
        ii = i
        if (k < mpow(i)) goto 7
        k = k - mpow(i)
    enddo

    7 k = k + mpow(ii)
enddo

! final steps deal with dt factors
if (zign > 0.) then ! FORWARD FFT

    xi(1:lx) = xi(1:lx) * dtt ! multiplication by dt

else ! REVERSE FFT

    flx = flx * dtt
    inv_of_flx = 1._CUSTOM_REAL / flx

    !! DK DK Aug 2016: changed to multiplication by the precomputed inverse to make the routine faster
    !       xi(1:lx) = xi(1:lx) / flx         ! division by dt
    xi(1:lx) = xi(1:lx) * inv_of_flx ! division by dt

endif

end subroutine FFT

!
!-------------------------------------------------------------------------------------------------
!

subroutine FFTinv(npow, s, zign, dtt, r, mpow)

    ! inverse Fourier transform -- calls FFT

    use constants

    implicit none

    integer, parameter :: CUSTOM_CMPLX = 8

    integer :: npow, nsmp, nhalf
    real(kind = CUSTOM_REAL) :: dtt, zign

    complex(kind = CUSTOM_CMPLX), intent(in) :: s(*)
    real(kind = CUSTOM_REAL), intent(out) :: r(*) ! note that this is real, not double precision

    !! DK DK here is the hardwired maximum size of the array
    !! DK DK Aug 2016: if this routine is called many times (for different mesh points at which the SEM is coupled with FK)
    !! DK DK Aug 2016: this should be moved to the calling program and precomputed once and for all
    real(kind = CUSTOM_REAL) :: mpow(30)

    nsmp = 2**npow
    nhalf = nsmp/2

    call rspec(s, nhalf) ! restructuring
    call FFT(npow, s, zign, dtt, mpow) ! Fourier transform

    r(1:nsmp) = real(s(1:nsmp)) ! take the real part

end subroutine FFTinv

!
!-------------------------------------------------------------------------------------------------
!

subroutine rspec(s, np2)

    implicit none

    integer, parameter :: CUSTOM_CMPLX = 8

    integer :: np2, n, n1, i

    complex(kind = CUSTOM_CMPLX) :: s(*)

    n = 2 * np2
    n1 = np2 + 1

    s(n1) = 0.0
    s(1) = cmplx(real(s(1)), 0.0)

    do i = 1, np2
        s(np2 + i) = conjg(s(np2 + 2 - i))
    enddo

end subroutine rspec

!
!-------------------------------------------------------------------------------------------------
!

subroutine find_size_of_working_arrays(deltat, tmax_fk, NF_FOR_STORING, &
    NF_FOR_FFT, NPOW_FOR_INTERP, np_resampling, DF_FK)

    use constants

    real(kind = CUSTOM_REAL), intent(inout) :: tmax_fk
    real(kind = CUSTOM_REAL), intent(inout) :: DF_FK, deltat
    integer, intent(inout) :: NF_FOR_STORING, NF_FOR_FFT, NPOW_FOR_INTERP, np_resampling

    real(kind = CUSTOM_REAL) :: df, dt_min_fk, Frq_ech_Fk

    !! sampling frequency to store fk solution
    Frq_ech_Fk = 20._CUSTOM_REAL !! WM WM TO DO PUT THIS IN PARAMETER

    !! sampling time step to store fk solution
    dt_min_fk = 1. / Frq_ech_Fk
    
    !!  compute resampling rate
    np_resampling = floor(dt_min_fk / deltat)
    !np_resampling = 4

    !! sampling time step to store fk solution
    dt_min_fk = np_resampling*deltat

    !! sampling frequency to store fk solution
    !Frq_ech_Fk = 1/dt_min_fk !! WM WM TO DO PUT THIS IN PARAMETER

    !! compute number of time steps to store
    NF_FOR_STORING = ceiling(tmax_fk / dt_min_fk)

    !! in power of two
    NF_FOR_STORING = ceiling(log(real(NF_FOR_STORING))/log(2.))

    !! multiply by 2 in order to do an inverse FFT
    NF_FOR_FFT = 2** (NF_FOR_STORING + 1)

    NPOW_FOR_INTERP = NF_FOR_STORING + 1
    NF_FOR_STORING = 2** NF_FOR_STORING

    !! now we have this new time window
    tmax_fk = dt_min_fk * (NF_FOR_FFT - 1)

    !! step in frequency for fk
    df = 1. / tmax_fk

    DF_FK = df

end subroutine find_size_of_working_arrays


!
!-------------------------------------------------------------------------------------------------
!

!! #################  INTERPOLATION ROUTINES IN TIME DOMAIN ######################################

!! compute and store spline coefficients

subroutine compute_spline_coef_to_store(Sig, npts, spline_coeff)

    use constants
    implicit none
    integer, intent(in) :: npts
    real(kind = CUSTOM_REAL), dimension(npts), intent(in) :: Sig
    real(kind = CUSTOM_REAL), dimension(npts), intent(inout) :: spline_coeff


    !! computation in double precision
    double precision :: error = 1.d-24
    double precision :: z1, zn, sumc
    double precision, dimension(:), allocatable :: c
    integer :: i, n_init, ier

    allocate(c(npts), stat = ier)
    if (ier /= 0) call exit_MPI_without_rank('error allocating array 2225')

    ! Compute pole value
    z1 = dsqrt(3.d0) - 2.d0
    c(:) = dble(Sig(:)) * (1.d0 - z1) *(1.d0 - 1.d0/ z1)

    ! Initialisation causal filter
    n_init = ceiling(log(error)/log(abs(z1)))
    sumc = c(1)
    zn = z1
    do i = 1, n_init
        sumc = sumc + zn * c(i)
        zn = zn * z1
    enddo
    c(1) = sumc

    ! Causal filter
    do i = 2, npts
        c(i) = c(i) + z1 * c(i - 1)
    enddo

    ! Initialisation anti-causal filter
    c(npts) = (z1 / (z1 - 1.d0)) * c(npts)
    do i = npts - 1, 1, -1
        c(i) = z1 * (c(i + 1) - c(i))
    enddo

    !! store spline coeff in CUSTOM_REAL precision
    spline_coeff(:) = c(:)

    deallocate(c)

end subroutine compute_spline_coef_to_store


!
!-------------------------------------------------------------------------------------------------
!

!! VM VM READING INPUT FILE FOR FK MODEL

subroutine ReadFKModelInput(Xmin_box, Xmax_box, Ymin_box, Ymax_box, Zmin_box)

    use specfem_par
    use specfem_par_coupling
    use constants

    implicit none

    real(kind = CUSTOM_REAL), intent(in) :: Xmin_box, Xmax_box, Ymin_box, Ymax_box, Zmin_box
    integer :: ioerr
    character(len = 100) :: keyword, keyvalue, line
    character(len = 100) :: keyword_tmp, incident_wave

    real(kind = CUSTOM_REAL) :: rho_layer, vp_layer, vs_layer, ztop_layer
    real(kind = CUSTOM_REAL) :: deg2rad, Radius_box, wave_length_at_bottom
    real(kind = CUSTOM_REAL), dimension(:), allocatable :: rho_fk_input, vp_fk_input, vs_fk_input, ztop_fk_input
    integer, dimension(:), allocatable :: ilayer_fk_input
    integer :: ilayer, ier
    logical :: position_of_wavefront_not_read

    !!--------------------------------------------------------------
    ! # model description :
    ! NLAYER   n # number of layers
    ! LAYER 1  rho, vp ,vs, ztop
    ! LAYER 2  rho, vp, vs, ztop
    ! ...
    ! LAYER n  rho, vp, vs, ztop  # homogenoeus half-space
    ! #
    ! # incident wave description:
    ! INCIDENT_WAVE  "p" or "sv"
    ! BACK_AZITUTH    bazi
    ! INCIDENCE       inc
    ! ORIGIN_WAVEFRONT xx0, yy0, zz0
    ! ORIGIN_TIME      tt0
    ! FREQUENCY_MAX    ff0
    ! TIME_WINDOW      tmax_fk
    !!----------------------------------------------------------------

    ! only master process reads
    if (myrank /= 0) return

    !! set default values
    tt0 = 0.
    tmax_fk = 128.
    fmax_fk = 0.
    td = 0.
    kpsv = 1 ! 1 == P-wave / 2 == SV-wave
    position_of_wavefront_not_read = .true.
    stag = .false.
    deg2rad = pi/180.d0
    isw = 1

    !! READING input file
    open(85, file = trim(FKMODEL_FILE))
    do
        read(85, fmt = '(a100)', iostat = ioerr) line
        !call remove_begin_blanks(line)
        if (ioerr < 0) exit
        if (len(trim(line)) < 1 .or. line(1:1) == '#') cycle
        read(line, *) keyword, keyvalue
        select case(trim(keyword))

        case('NLAYER')
            read(line, *) keyword_tmp, nlayer
            allocate(al_FK(nlayer), be_FK(nlayer), mu_FK(nlayer), h_FK(nlayer), stat = ier)
            if (ier /= 0) call exit_MPI_without_rank('error allocating array 2226')
            allocate(rho_fk_input(nlayer), stat = ier)
            if (ier /= 0) call exit_MPI_without_rank('error allocating array 2227')
            allocate(vp_fk_input(nlayer), stat = ier)
            if (ier /= 0) call exit_MPI_without_rank('error allocating array 2228')
            allocate(vs_fk_input(nlayer), stat = ier)
            if (ier /= 0) call exit_MPI_without_rank('error allocating array 2229')
            allocate(ztop_fk_input(nlayer + 1), stat = ier)
            if (ier /= 0) call exit_MPI_without_rank('error allocating array 2230')
            allocate(ilayer_fk_input(nlayer + 1), stat = ier)
            if (ier /= 0) call exit_MPI_without_rank('error allocating array 2231')
            ilayer_fk_input(:) = -1

        case('LAYER')
            read(line, *) keyword_tmp, ilayer, rho_layer, vp_layer, vs_layer, ztop_layer
            ilayer_fk_input(ilayer) = ilayer
            rho_fk_input(ilayer) = rho_layer
            vp_fk_input(ilayer) = vp_layer
            vs_fk_input(ilayer) = vs_layer
            ztop_fk_input(ilayer) = ztop_layer

        case('INCIDENT_WAVE')
            read(line, *) keyword_tmp, incident_wave

            select case(trim(incident_wave))
        case ('p', 'P')
            kpsv = 1
        case('sv', 'SV')
            kpsv = 2
        case('sh', 'SH')
            kpsv = 3
        case default
            kpsv = 1
        end select

        case('BACK_AZIMUTH')
            read(line, *) keyword_tmp, phi_FK
            phi_FK = -phi_FK - 90.

        case('AZIMUTH')
            read(line, *) keyword_tmp, phi_FK
            phi_FK = 90. - phi_FK

        case('TAKE_OFF')
            read(line, *) keyword_tmp, theta_FK

        case('ORIGIN_WAVEFRONT')
            read(line, *) keyword_tmp, xx0, yy0, zz0
            position_of_wavefront_not_read = .false.

        case('ORIGIN_TIME')
            read(line, *) keyword_tmp, tt0

        case('SOURCE_WAVELET')
            read(line, *) keyword_tmp, isw

        case('DOMINANT_PERIOD')
            read(line, *) keyword_tmp, td

        case('FREQUENCY_MAX')
            read(line, *) keyword_tmp, fmax_fk

        case('TIME_WINDOW')
            read(line, *) keyword_tmp, tmax_fk

        end select
        !!------------------------------------------------------------------------------------------------------
    enddo

    !Check if Td was define in the FKMODEL file
    if(td == 0.) then
        write(*,*)"ERROR DOMINANT PERIOD IS NOT DEFINED IN ", FKMODEL_FILE
        stop
    endif

   !if max frequency is not defined, compute it:
    if (fmax_fk == 0.0) then
        !Gauss
        if(isw == 1) then
            fmax_fk = 4. / td
        endif
    endif    

    if (allocated(ilayer_fk_input)) then

        ilayer_fk_input(nlayer + 1) = ilayer_fk_input(nlayer)
        ztop_fk_input(nlayer + 1) = ztop_fk_input(nlayer)
        Z_ref_for_FK = ztop_fk_input(nlayer)
        do ilayer = 1, nlayer
            al_FK(ilayer) = vp_fk_input(ilayer)
            be_FK(ilayer) = vs_fk_input(ilayer)
            mu_FK(ilayer) = rho_fk_input(ilayer) * vs_fk_input(ilayer)**2
            h_FK(ilayer) = ztop_fk_input(ilayer) - ztop_fk_input(ilayer + 1)

            if (ilayer_fk_input(ilayer) == -1) then
                write(*, *) " ERROR READING FK INPUT FILE "
                write(*, *) " MISSING LAYER ", ilayer
                stop
            endif
        enddo

        deallocate(ilayer_fk_input, rho_fk_input, vp_fk_input, vs_fk_input, ztop_fk_input)

    else

        write(*, *) " ERROR READING FK INPUT FILE "
        write(*, *) " NOT BE ABLE TO READ MODEL PROPERTIES "
        stop

    endif

    !! compute position of wave front
    if (position_of_wavefront_not_read) then
        xx0 = 0.5 * (Xmin_box + Xmax_box)
        yy0 = 0.5 * (Ymin_box + Ymax_box)
        Radius_box = sqrt((Xmin_box - xx0)**2 + (Ymin_box - yy0)**2)

        if (kpsv == 1) then
            wave_length_at_bottom = al_FK(nlayer) * td 
        else if (kpsv == 2 .or. kpsv == 3) then
            wave_length_at_bottom = be_FK(nlayer) * td 
        endif

! Old definition
        zz0 = Zmin_box - Radius_box * tan(abs(theta_FK) * (acos(-1.d0) / 180.d0)) - &
         3. * wave_length_at_bottom * cos(abs(theta_FK) * (acos(-1.d0) / 180.d0)) - &
         Z_ref_for_FK

! New definition
!        zz0 = Zmin_box * cos(abs(theta_FK) * deg2rad)**2 & 
!        - 0.5 * Radius_box * sin(abs(2*theta_FK) * deg2rad) & 
!        - 3.0 * wave_length_at_bottom * cos(abs(theta_FK) * deg2rad) &
!        - Z_ref_for_FK

    endif

    write(IMAIN, *) " ********************************************** "
    write(IMAIN, *) "         USING FK INJECTION TECHNIQUE           "
    write(IMAIN, *) " ********************************************** "

    write(IMAIN, *)
    write(IMAIN, *) "         Model : ", nlayer, " layers "
    write(IMAIN, *)

    do ilayer = 1, nlayer
        write(IMAIN, '(a7, i3, 3(a6,2x,f8.3), 3x, a9, f18.5 )') &
        'layer ', ilayer, &
        " rho  =", mu_FK(ilayer) / be_FK(ilayer)**2, &
        " vp   =", al_FK(ilayer), &
        " vs   =", be_FK(ilayer), &
        " Height =", h_FK(ilayer)
    enddo

    write(IMAIN, *) " Xmin Xmax  : ", Xmin_box, Xmax_box
    write(IMAIN, *) " Ymin Ymax  : ", Ymin_box, Ymax_box
    write(IMAIN, *) " Zmin       : ", Zmin_box
    write(IMAIN, *) " Radius_box : ", Radius_box


    write(IMAIN, *)
    write(IMAIN, *)
    write(IMAIN, *) " FK phi   = ", phi_FK, '(deg)'
    write(IMAIN, *) " FK theta = ", theta_FK, '(deg)'
    write(IMAIN, *)
    write(IMAIN, *) " Origin wavefront point FK  : ", xx0, yy0, zz0
    write(IMAIN, *) " time shift  FK             : ", tt0
    write(IMAIN, *) " Z reference for FK routine : ", Z_ref_for_FK
    write(IMAIN, *) " Window for FK computing    : ", tmax_fk
    write(IMAIN, *) " Type of wavelet (1=gaussian), (2 = ..) :", isw
    write(IMAIN, *) " dominant period            : ", td
    write(IMAIN, *) " frequency max              : ", fmax_fk
    write(IMAIN, *) " type of incoming wave (1=p), (2=sv), (3=sh) :", kpsv
    write(IMAIN, *)
    write(IMAIN, *)

    call flush_IMAIN()

end subroutine ReadFKModelInput

!
!-------------------------------------------------------------------------------------------------
!

subroutine FindBoundaryBox(Xmin_box, Xmax_box, Ymin_box, Ymax_box, Zmin_box, Zmax_box)

    use specfem_par

    implicit none
    real(kind = CUSTOM_REAL), intent(in out) :: Xmin_box, Xmax_box, Ymin_box, Ymax_box, Zmin_box, Zmax_box
    real(kind = CUSTOM_REAL) :: Xmin_loc, Xmax_loc, Ymin_loc, Ymax_loc, Zmin_loc, Zmax_loc

    Xmin_loc = minval(xstore(:))
    Xmax_loc = maxval(xstore(:))
    Ymin_loc = minval(ystore(:))
    Ymax_loc = maxval(ystore(:))
    Zmin_loc = minval(zstore(:))
    Zmax_loc = maxval(zstore(:))

    call min_all_all_cr(Xmin_loc, Xmin_box)
    call max_all_all_cr(Xmax_loc, Xmax_box)
    call min_all_all_cr(Ymin_loc, Ymin_box)
    call max_all_all_cr(Ymax_loc, Ymax_box)
    call min_all_all_cr(Zmin_loc, Zmin_box)
    call max_all_all_cr(Zmax_loc, Zmax_box)

end subroutine FindBoundaryBox


!
!-------------------------------------------------------------------------------------------------
!

