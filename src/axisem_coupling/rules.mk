## compilation directories
S := ${S_TOP}/src/axisem_coupling
$(axisem_coupling_OBJECTS): S := ${S_TOP}/src/axisem_coupling


axisem_coupling_TARGETS = \
	$E/xexpand_2D_3D \
	$E/xreformat \
	$E/create_stations.x \
	$(EMPTY_MACRO)


axisem_coupling_OBJECTS = \
	$(xexpand_2D_3D_OBJECTS) \
	$(xreformat_OBJECTS) \
	$(create_stations_OBJECTS) \
	$(EMPTY_MACRO)

xexpand_2D_3D_OBJECTS = \
	$O/global_parameters_mod.axi.o \
	$O/rotation_matrix_mod.axi.o \
	$O/mpi_mod.axi.o \
	$O/reading_inputs_mod.axi.o \
	$O/interpol_mod.axi.o \
	$O/post_process_mod.axi.o \
	$O/writing_mod.axi.o \
	$O/reading_fields_mod.axi.o \
	$O/gll_library.axi.o \
	$O/program_expand_2D_to_3D.axi.o \
	$(EMPTY_MACRO)

xreformat_OBJECTS = \
	$O/reformat_output_files.axi.o \
	$(EMPTY_MACRO)

create_stations_OBJECTS = \
	$O/create_stations.axi.o \
	$(EMPTY_MACRO)

axisem_coupling_MODULES = \
	$(FC_MODDIR)/global_parameters_mod.$(FC_MODEXT) \
	$(FC_MODDIR)/interpol_mod.$(FC_MODEXT) \
	$(FC_MODDIR)/mpi_mod.$(FC_MODEXT) \
	$(FC_MODDIR)/post_process_mod.$(FC_MODEXT) \
	$(FC_MODDIR)/reading_fields_mod.$(FC_MODEXT) \
	$(FC_MODDIR)/reading_inputs_mod.$(FC_MODEXT) \
	$(FC_MODDIR)/rotation_matrix_mod.$(FC_MODEXT) \
	$(FC_MODDIR)/writing_mod.$(FC_MODEXT) \
	$(EMPTY_MACRO)


axi: $(axisem_coupling_TARGETS)

expand_2D_3D: xexpand_2D_3D
xexpand_2D_3D: $E/xexpand_2D_3D

reformat: xreformat
xreformat: $E/xreformat

create_stations: create_stations.x
create_stations.x: $E/create_stations.x

$E/xexpand_2D_3D: $(xexpand_2D_3D_OBJECTS)
	${FCLINK} -o $@ $+

${E}/xreformat: $(xreformat_OBJECTS)
	${FCLINK} -o $@ $+

${E}/create_stations.x: $(create_stations_OBJECTS)
	${FCLINK} -o $@ $+

$O/%.axi.o: $S/%.f90
	${MPIFCCOMPILE_CHECK} ${FCFLAGS_f90} $(MPI_INCLUDES) -c -o $@ $<
