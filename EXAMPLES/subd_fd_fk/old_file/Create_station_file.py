#!/usr/bin/env python
# -*- coding: utf-8 -*-


################### Path to PySpecfem ########################################
PySpecfemDir="/home/plab/COUPLING_AXISEM_SPECFEM/PySpecfem"
##############################################################################
import sys


# add path before importing PySpecfem tools
sys.path.append(PySpecfemDir)
sys.path.append(PySpecfemDir+"/AxisemCoupling")
#print sys.path


from AxisemCoupling.station_grid import StationGrid


print ""
print " Creating stations file "
print ""

# longitude - latitude - elevation
station_origin=[-73.5, -33.5, 0.0]
station_step=[0.025, 1.0, 1.0]
station_number=[121, 1, 1]
directory_for_data="./"

print station_origin

print station_origin[0]

sta=StationGrid()
sta.set_parameters(station_origin, station_step, station_number, directory_for_data)
sta.write_file()
