#!/usr/bin/env python
#-*- coding: utf-8 -*-


#  
# File:   Get_FK_angles.py
# Author: Plazolles Bastien
#
# Created on 21 Aout 2019, 16:18

#Compute incident and back Azimuth angles for FK injection from GCMT files
#To be add in the PySpecfem workflow

## Import
import numpy as np
import os
import sys
import math
from obspy.geodetics import gps2dist_azimuth
from obspy.taup import TauPyModel


if __name__ == "__main__":
	
	
	#Get informations on specfem simulation box center location
	SpecSetupFile = 'parameters_subduction.in'
	with open(SpecSetupFile) as f:
		SimuPar = f.readlines()
	
	
	
	for line in SimuPar:
		if 'LONGITUDE_CENTER_OF_CHUNK' in line :
			longitudeBox = float(line.split(':')[1]) 
		if 'LATITUDE_CENTER_OF_CHUNK' in line :
			latitudeBox = float(line.split(':')[1])
		if 'fk_nb_layer' in line :
			fk_layer = line.split(':')[1]
			for newline in SimuPar:
				if 'fk_layer : %s'%(fk_layer.strip()) in newline:
					vp = float(newline.split(' ')[-3])
					vs = float(newline.split(' ')[-2])
					print(vp, vs)
#		For future version when INCIDENT_WAVE type will be avaiable in Specfem setup file
#		if 'INCIDENT_WAVE' in line :
#			wave_type = line.split(':')[1]			
	wave_type = 'p'
	
	print("Box center")
	print(longitudeBox,latitudeBox)
		
	#Load ak135 model (to change consider: https://docs.obspy.org/packages/obspy.taup.html?highlight=taup#module-obspy.taup )
	model = TauPyModel(model="ak135")
	
	#Load GCMT file
	GCMTFileName = 'CMTSOLUTION_ev1'
	
	with open(GCMTFileName) as f:
		GCMTfile=f.readlines()
		
	for line in GCMTfile:	
		if 'latitude' in line:
			latitudeSour = float(line.split(':')[1])
		if 'longitude' in line:
			longitudeSour = float(line.split(':')[1])
		if 'depth' in line :
			depthSour = float(line.split(':')[1])
			
	print("Source =")
	print(longitudeSour,latitudeSour,depthSour)
			
	#Compute distance between source and center of specfem box and back Azimuth		
	val = gps2dist_azimuth(latitudeSour,longitudeSour, latitudeBox, longitudeBox)
	val_corr = gps2dist_azimuth(latitudeSour, longitudeSour, latitudeBox, longitudeBox,a=6378137.0, f=0.0033528106647474805)
	
	#Back azimuth in degrees
	backAz = val[2] 
	backAz_cor = val_corr[2]
	
	
	#Great circle distance in m
	grDistm = val[0]
	grDistm_corr = val_corr[0]
	
	#Great dist in °
	grDistDeg = grDistm / (111.19 * 1000.0)
	
	print("Distance")
	print("%lf\n"%(grDistDeg))
	
	grDistDeg_corr = grDistm_corr / (111.19 * 1000.0)
		
	arrivals = model.get_travel_times(source_depth_in_km=depthSour, distance_in_degree = grDistDeg, phase_list = [wave_type.upper()[0]])
	arrivals_corr = model.get_travel_times(source_depth_in_km=depthSour, distance_in_degree = grDistDeg_corr, phase_list = [wave_type.upper()[0]])
	
	#~ arrivals = model.get_travel_times(source_depth_in_km=depthSour, distance_in_degree = grDistDeg, phase_list = ["P"])
	
	print(arrivals)
	print(arrivals[0])
	
	arr = arrivals[0]
	arr_corr = arrivals_corr[0]
	
	
	arrTime = arr.time
	print("Fenetre de temps AxiSEM = %f begin, %f end"%(arrTime - 30, arrTime + 30))
	
	
	# Ray parameter un s/m
	rayParam = arr.ray_param * ( math.pi / 180.0) / (111.19 * 1000.0)
	rayParam_corr = arr_corr.ray_param * ( math.pi / 180.0) / (111.19 * 1000.0)
		
	#Incident angle at surface of the box	
	angleInc_surf = arr.incident_angle
	angleInc_corr_suf = arr_corr.incident_angle
	
	if wave_type == 'p' :
		angleInc = math.degrees(math.asin(rayParam * vp))
		angleInc_corr = math.degrees(math.asin(rayParam_corr * vp))
	if wave_type[0] == 's' : 
		angleInc = math.degrees(math.asin(rayParam * vs))
		angleInc_corr = math.degrees(math.asin(rayParam_corr * vs))
	
	print("Incident wave = %s, Incident angle = %f, Back Azimuth = %f"%(wave_type.upper()[0],angleInc,backAz)) #To write at the right place in FKMODEl file
	print("Incident wave corr = %s, Incident angle corr = %f, Back Azimuth corr = %f"%(wave_type.upper()[0],angleInc_corr,backAz_cor)) #To write at the right place in FKMODEl file
	
	
