#!/usr/bin/env bash 

cd axisem_run/SOLVER/RUN1
cp ../../../in_files/coupling_inputs/reformat.par ../.
cp ../../../../../EXTERNAL_PACKAGES_coupled_with_SPECFEM3D/AxiSEM_for_SPECFEM3D/UTILS_COUPLING_SpecFEM/xreformat .
mpirun -np 8  ./xreformat 
cd ../../
