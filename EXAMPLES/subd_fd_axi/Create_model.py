#!/usr/bin/env python
#-*- coding: utf-8 -*-


#  
# File:   Create_model.py
# Author: Plazolles Bastien
#
# Created on 23 Juillet 2019, 13:31
# 
#  Revision date:
#  Revised by :
#  Modification : 
#
#
#  Based on:
#  Create_model.m by Vadim Monteiller
#
#  Create model rho, vp, vs in regular grid and store it in binary files in
#  disk. 
#  
#  
#


## Import
import numpy as np
import math
import struct
import pickle
import sys
try:
    from cStringIO import StringIO 
except ImportError:
    from io import StringIO
import getopt


def printUsageAndExit(script):
    print("Usage %s [options]"%(script))
    print( "-h  | --help					Print the usage message")
    sys.exit()



def fz3(y):
    z = -0.00004*(y+60.)**3 + 0.01*(y+60.)**2
    return -z


def fz4(x, y):
  x0=10.0
  y0=30.0
  sx = 25.0
  sy = 13.0
  A = 8.0
  return -A * np.exp( -0.5 * ( ((x-x0)/sx)**2.0 + ((y-y0)/sy)**2) ) +0.2


if __name__ == "__main__":


    try:
        opts, args = getopt.getopt(sys.argv[1:],"h:",["help"])
    except getopt.GetoptError:
        print( sys.argv[0]+'-h')
        sys.exit(2)
    
    test = 0
    
    for opt, arg in opts:
        if opt in ("-h","--help"):
            printUsageAndExit(sys.argv[0])
    
    # box boundary		
    xmin=-167000.
    xmax= 167000.
    ymin=-80000.
    ymax= 80000.
    
    zmin=-250000.0000
    zmax=  10000.0000
     
    # fd grid
    nx=501
    ny=321
    nz=501
    
    hx = (xmax-xmin) / (nx-1)
    hy = (ymax-ymin) / (ny-1)
    hz = (zmax-zmin) / (nz-1)
    
    # initialize models (true model)
    RHO=3000*np.ones((nx,ny,nz))
    VP=5000*np.ones((nx,ny,nz))
    VS=4000*np.ones((nx,ny,nz))
    
    # initial smooth model 
    rho_smooth=RHO.copy()
    vp_smooth=VP.copy()
    vs_smooth=VS.copy()
    
    X=np.linspace(xmin,xmax,num=nx)
    Y=np.linspace(ymin,ymax,num=ny)
    Z=np.linspace(zmin,zmax,num=nz)
    
    
    # define model parameters 
    vp1=5800.
    vp2=6500.
    vp3=8000.
    vp4=4000
    
    vs1=3200
    vs2=3900
    vs3=4480
    vs4=2800
    
    rho1=2600.
    rho2=2900.
    rho3=3500.
    rho4=1800
    
    # initialize arrays 
    vp=vp3*np.ones((nx,nz))
    vs=vs3*np.ones((nx,nz))
    rho=rho3*np.ones((nx,nz))
    
    
    vp_ref=vp.copy()
    vs_ref=vs.copy()
    rho_ref=rho.copy()
    
    print("Generate the models")
    
    # construct the model 
    for k in range(nz):
        z = Z[k]/1000.
        for i in range(nx):
            x = X[i]/1000.
            
            if (z <= 0):
                # default model----------------------------------------------
                if (z > -20):
                    vp[i,k]=vp1
                    vs[i,k]=vs1
                    rho[i,k]=rho1
                
                if ( z<=-20 and z > -35):
                    vp[i,k]=vp2
                    vs[i,k]=vs2
                    rho[i,k]=rho2
                    
                if (z <-35):
                    vp[i,k]=vp3
                    vs[i,k]=vs3
                    rho[i,k]=rho3
                
                rho_ref[i,k]=rho[i,k]
                vp_ref[i,k]=vp[i,k]
                vs_ref[i,k]=vs[i,k]
                
                # slab structure -------------------------------------------
                if ( x > -50 and z < fz3(x) and z > fz3(x) -20. and x < 65):
                    vp[i,k]=vp1
                    vs[i,k]=vs1
                    rho[i,k]=rho1
                
                if (x > -50 and z <= fz3(x) -20. and z > fz3(x) -35. and x < 65):
                    vp[i,k]=vp2
                    vs[i,k]=vs2
                    rho[i,k]=rho2
                    
                # bassin--------------------------------------------------------
                if (z > fz4(10,x)):
                    vp[i,k]=vp4
                    vs[i,k]=vs4
                    rho[i,k]=rho4
        
                # repeat the same in x direction ------
                for j in range(ny):
                    
                    # apply taper on sides of the box
                    if(j < 20 or j > ny-20):
                    
                        VP[i,j,k] = vp_ref[i,k]
                        VS[i,j,k] = vs_ref[i,k]
                        RHO[i,j,k] = rho_ref[i,k]
                                        
                    else:	
                        VP[i,j,k]=vp[i,k]
                        VS[i,j,k]=vs[i,k]
                        RHO[i,j,k]=rho[i,k]
                        
                        vp_smooth[i,j,k]=vp_ref[i,k]
                        vs_smooth[i,j,k]=vs_ref[i,k]
                        rho_smooth[i,j,k]=rho_ref[i,k]
            else:
                 # repeat the same in x direction ------
                for j in range(ny):
                    
                    VP[i,j,k]=VP[i,j,k-1]
                    VS[i,j,k]=VS[i,j,k-1]
                    RHO[i,j,k]=RHO[i,j,k-1]
                    
                    vp_smooth[i,j,k]=vp_smooth[i,j,k-1]
                    vs_smooth[i,j,k]=vs_smooth[i,j,k-1]
                    rho_smooth[i,j,k]=rho_smooth[i,j,k-1]
    
    
    print("Write the models")
    
    # write model in disk
    with open('model_vp_simple_subduction.bin', 'wb') as fout:
        for a in VP.ravel(order='f'):
            fout.write(struct.pack('f', a))
            
            
    with open('model_vs_simple_subduction.bin', 'wb') as fout:
        for a in VS.ravel(order='f'):
            fout.write(struct.pack('f', a))
            
            
    with open('model_rho_simple_subduction.bin', 'wb') as fout:
        for a in RHO.ravel(order='f'):
            fout.write(struct.pack('f', a))
                    
    #write input model in disk
    with open('model_rho_smooth.bin','wb') as fout:
        for a in rho_smooth.ravel(order='f'):
            fout.write(struct.pack('f', a))
    
    with open('model_vp_smooth.bin','wb') as fout:
        for a in vp_smooth.ravel(order='f'):
            fout.write(struct.pack('f', a))
    
    with open('model_vs_smooth.bin','wb') as fout:
        for a in vs_smooth.ravel(order='f'):
            fout.write(struct.pack('f', a))
                    
    
    ## write grid parameters for generate databases

    # for true model 
    fid=open("fd_grid.txt_true","w")
    fid.write(" %f %f %f\n"%(xmin, ymin, zmin))
    fid.write(" %f %f %f\n"%(hx, hy, hz))
    fid.write(" %d %d %d\n"%(nx, ny, nz))
    fid.write("model_rho_simple_subduction.bin\n")
    fid.write("model_vp_simple_subduction.bin\n")
    fid.write("model_vs_simple_subduction.bin\n")
    fid.close()
    
    # for smooth initial model
    fid=open("fd_grid.txt_init","w")
    fid.write(" %f %f %f \n"%(xmin, ymin, zmin))
    fid.write(" %f %f %f \n"%(hx, hy, hz))
    fid.write(" %d %d %d \n"%(nx, ny, nz))
    fid.write("model_rho_smooth.bin\n")
    fid.write("model_vp_smooth.bin\n")
    fid.write("model_vs_smooth.bin\n")
    fid.close()



