#!/usr/bin/env bash 

echo "running example: `date`"
currentdir=`pwd`

# sets up directory structure in current example directory
echo
echo "   setting up example..."
echo

# cleans output files
mkdir -p OUTPUT_FILES
mkdir -p DATABASES_MPI
mkdir -p MESH
rm -rf OUTPUT_FILES/*
rm -rf DATABASES_MPI/*
rm -rf axisem_run
rm -rf MESH/*
rm -rf DATA/AxiSEM_tractions
mkdir -p DATA/AxiSEM_tractions/1

# copy some inputs
cp in_files/coupling_inputs/ParFileMeshChunk MESH/.
cp in_files/coupling_inputs/ak135 MESH/.

# step 1/ run specfem mesher
./run_mesh.sh
./run_generate.sh

# step 2/ run axisem mesher
./run_axisem_mesher.sh

# step 3/ run axisem solver
./run_axisem_solver.sh

# step 4/ compute traction on specfem boundary with axisem solution
./run_expand_2D_3D.sh
./run_reformat.sh


# step 5/ run specfem 
./run_specfem.sh


# make some snapshots 
./create_one_snapshot.sh 250
./create_one_snapshot.sh 500
./create_one_snapshot.sh 750
./create_one_snapshot.sh 1000
./create_one_snapshot.sh 1250
./create_one_snapshot.sh 1500
./create_one_snapshot.sh 1750
./create_one_snapshot.sh 2000

./create_model_vtk.sh rho
./create_model_vtk.sh vp
./create_model_vtk.sh vs
