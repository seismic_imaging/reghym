
# clean previous run
\rm -rf axisem_run

# create directory for axisem run
mkdir axisem_run
cd axisem_run

#copy axisem package
cp -r ../../../EXTERNAL_PACKAGES_coupled_with_SPECFEM3D/AxiSEM_for_SPECFEM3D/AxiSEM_modif_for_coupling_with_specfem/* .


# copy inputs files for bith MESHER and SOLVER (legacy files)
cp ../in_files/axisem_inputs/inparam_mesh MESHER/.
cp ../in_files/axisem_inputs/inparam_basic SOLVER/.
cp ../in_files/axisem_inputs/inparam_advanced SOLVER/.
cp ../in_files/axisem_inputs/inparam_hetero SOLVER/.
cp ../in_files/axisem_inputs/inparam_source SOLVER/.

# copy additional files required for coupling
cd SOLVER
cp ../../MESH/list_ggl* .
# change name and format of files
./add_line_number_in_points_lists.sh

# go to mesher directory 
cd ../MESHER

# run the mesher legacy axisem script
./submit.csh

# move the mesh for solver 
./movemesh.csh MESH
cd ../../
